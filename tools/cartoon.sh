#!/usr/bin/env bash
# Copyright (C) 2021 Bart De Roy
# licence: MIT

set -o pipefail -o errexit
trap '[[ "$?" -gt 0 ]]' EXIT
trap 'exit 1' ERR

# Please make new cartoons as a seperate layer in "src/girl-inkscape.svg" using Inkscape (version 1.0 or newer).
# Make sure to make your cartoon the only visible layer.

[[ -f $1 ]] && file="$1"

# https://github.com/RazrFalcon/SVGCleaner
svgcleaner --indent=2 \
  --coordinates-precision=2 \
  --properties-precision=2 \
  --transforms-precision=2 \
  --paths-coordinates-precision=2 \
  --convert-shapes=no \
  --remove-nonsvg-attributes=no \
  "${file:=../src/girl-inkscape.svg}" "${file:=../src/girl-inkscape.svg}"-clean.svg

sed -i -e 's/\s*stroke-width="2"//g' \
  -e 's/\s*stroke-line\w\+="\w\+"//g' \
  -e 's/\s*stroke="#000"\s*//g' \
  -e 's/\s*fill="none"\s*//g' \
  -e 's/\s*sodipodi:\w\+="\w\+"//g' \
  -e 's/\s*inkscape:group\w\+="\w\+"//g' \
  -e 's/\s*font-\w\+="\w\+"//g' \
  -e 's/inkscape:label=/class="girl" id=/' "${file:=../src/girl-inkscape.svg}"-clean.svg

echo "Please edit ${file:=../src/girl-inkscape.svg}-clean.svg to fix errors and add css classes."

