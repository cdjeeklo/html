#!/bin/bash
for cmd in find sed markdown_py; do
  if ! command -v ${cmd} &> /dev/null ; then
    tput setaf 1
    >&2 echo Please install ${cmd}!
    tput sgr0
    exit 1
  fi
done
pip install -r requirements.txt

touch .cmd_ok
