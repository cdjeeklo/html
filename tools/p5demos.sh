#!/usr/bin/env bash
# Copyright (C) 2021 Bart De Roy
# licence: MIT

set -o pipefail -o errexit
trap 'exit 1' ERR

if [ ! -f /tmp/head.html ]; then
  cat <<EOF > /tmp/head.html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <script src="../../p5.js"></script>
    <style>
      html,body{margin:0;}
      main{text-align:center;}
      @media screen and (prefers-color-scheme: dark) {
        html{background-color: #111;}
      }
    </style>
  </head>
  <body>
    <script>
EOF
fi

if [ ! -f /tmp/tail.html ]; then
   cat <<EOF > /tmp/tail.html
    </script>
  </body>
</html>
EOF
fi


if [ -f $1 ]; then
  [[ $1 == index.md ]] && exit
  dir="${1%.*}"
  mkdir -p "$dir"
  cd "$dir"
  # <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/1.3.1/p5.js"></script>
  csplit -f '' -b "%02d.html" -q "../${1##*/}" '/^```/' '{*}'
  find . -type f -name '*.html' -exec bash -c \
    '[[ `head -n1 $0` == *js ]] && \
       sed -i -e '"'"'1r/tmp/head.html'"'"' -e '"'"'1d'"'"' \
              -e '"'"'$r/tmp/tail.html'"'"' $0 || \
       rm $0' '{}' \;
fi

