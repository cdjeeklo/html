# Simulatie 1

## Overzicht

<p class="overzicht r5 count" markdown="1">
[Krachten](#krachten)
[Deeltjes systeem](#deeltjes)
[School vissen](#vissen)
[Wolfram CA](#wolfram)
[Game of Life](#conway)
[Deeltjes systemen](#systemen)
[Spirograaf](#spirograaf)
[Spiraalveer](#veer)
[Zacht Lichaam](#zacht)
[Sneeuwvlokken](#sneeuw)
</p>

## Krachten { id=krachten }

Demonstratie van meerdere krachten die invloed uitoefenen op lichamen

<small markdown="1">Origineel door Daniel Shiffman, zie <http://natureofcode.com></small>

```javascript
// Demonstratie van meerdere krachten die invloed uitoefenen op lichamen (Lichaam class)
// Lichamen die continu zwaartekracht ervaren
// Lichamen die in een vloeistof weerstand ervaren

// Vijf bewegende lichamen
let lichamen = [];

// Vloeistof
let vloeistof;

function setup() {
  createCanvas(640, 360);
  // Maak een vloeistof object
  vloeistof = new Vloeistof(0, height / 2, width, height / 2, 0.1);
  // Maak vijf bewegende lichamen
  lichamen = Array.from({length: 5}, (_,i) =>
    new Lichaam(random(0.5, 3), 40 + i * 70, 0)
  );
  console.log(lichamen);
}

function draw() {
  background(127);

  // Teken vloeistof
  vloeistof.toon();

  lichamen.forEach(lichaam => {
    // Is het lichaam in de vloeistof?
    if (vloeistof.bevat(lichaam)) {
      // Bereken de kracht van de weerstand
      let weerstand = vloeistof.berekenWeerstand(lichaam);
      // Pas de kracht toe op het lichaam
      lichaam.zetKracht(weerstand);
    }

    // Zwaartekracht is hier het product
    // van 0.1 keer de massa van het lichaam!
    let zwaartekracht = createVector(0, 0.1 * lichaam.massa);
    // Pas de zwaartekracht toe
    lichaam.zetKracht(zwaartekracht);

    // Vernieuw en toon
    lichaam.vernieuw();
    lichaam.toon();
    lichaam.bots();
  });
}


function mousePressed() {
  lichamen.forEach(lichaam => {
    lichaam.m = random(0.5, 3);
    lichaam.positie.y = 0;
  });
}


class Vloeistof {
  constructor(x, y, w, h, c) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
  }

  // Is het lichaam in de vloeistof?
  bevat(m) {
    let l = m.positie;
    return l.x > this.x && l.x < this.x + this.w &&
          l.y > this.y && l.y < this.y + this.h;
  }

  // Bereken de kracht van de weerstand
  berekenWeerstand(m) {
    // De hoeveelheid is de coëfficiënt * snelheid tot de tweede
    let snelheid = m.snelheid.mag();
    let hoeveelheid = this.c * snelheid * snelheid;

    // Richting van de weerstand is omgekeerd aan de snelheid
    let weerstand = m.snelheid.copy();
    weerstand.mult(-1);

    // Schaal aan de hand van de hoeveelheid
    // weerstand.setMag(hoeveelheid);
    weerstand.normalize();
    weerstand.mult(hoeveelheid);
    return weerstand;
  }

  toon() {
    noStroke();
    fill(50);
    rect(this.x, this.y, this.w, this.h);
  }
}

class Lichaam{
  constructor(m, x, y) {
    this.massa = m;
    this.positie = createVector(x, y);
    this.snelheid = createVector(0, 0);
    this.versnelling = createVector(0, 0);
  }

  // 2de wet van Newton: F = M * A
  // of A = F / M
  zetKracht(force) {
    let f = p5.Vector.div(force, this.massa);
    this.versnelling.add(f);
  }

  vernieuw() {
    // Snelheid verandert met versnelling
    this.snelheid.add(this.versnelling);
    // positie verandert met snelheid
    this.positie.add(this.snelheid);
    // We moeten versnelling hier steeds weer nul maken
    this.versnelling.mult(0);
  }

  toon() {
    stroke(0);
    strokeWeight(2);
    fill(255,127);
    ellipse(this.positie.x, this.positie.y, this.massa * 16, this.massa * 16);
  }

  // Bots tegen de onderkant
  bots() {
    if (this.positie.y > (height - this.massa * 8)) {
      // Verlies een beetje snelheid
      this.snelheid.y *= -0.9;
      this.positie.y = (height - this.massa * 8);
    }
  }
}
```

<iframe loading="lazy" src="simulatie1/01.html" width="650px" height="370px" frameBorder="0"></iframe>

## Deeltjes systeem { id=deeltjes }

Dit is een eenvoudig systeem van deeltjes.

<small markdown="1">Origineel door Daniel Shiffman, zie <http://natureofcode.com></small>


```javascript
let systeem;

function setup() {
  createCanvas(720, 400);
  systeem = new deeltjesSysteem(createVector(width / 2, 50));
}

function draw() {
  background(51);
  systeem.nieuwDeeltje();
  systeem.toon();
}

// Definieer een deeltje
class Deeltje {
  constructor(positie) {
    this.versnelling = createVector(0, 0.05);
    this.snelheid = createVector(random(-1, 1), random(-1, 0));
    this.positie = positie.copy();
    this.levensduur = 255;
  }

  // Methode om de positie te veranderen
  vernieuw() {
    this.snelheid.add(this.versnelling);
    this.positie.add(this.snelheid);
    this.levensduur -= 2;
  }

  // Methode om het deeltje te tekenen
  toon() {
    stroke(200, this.levensduur);
    strokeWeight(2);
    fill(127, this.levensduur);
    ellipse(this.positie.x, this.positie.y, 12, 12);
  }

  // Is dit deeltje nog bruikbaar?
  isDood() {
    if (this.levensduur < 0.0) {
      return true;
    } else {
      return false;
    }
  }
}

class deeltjesSysteem {
  constructor(positie) {
    this.oorsprong = positie.copy();
    this.deeltjes = [];
  }

  nieuwDeeltje() {
    this.deeltjes.push(new Deeltje(this.oorsprong));
  }

  toon() {
    for (let deeltje of this.deeltjes) {
      deeltje.vernieuw();
      deeltje.toon();
    }

    // Array.filter() verwijdert dingen uit een lijstje
    // die niet voldoen aan de test.
    this.deeltjes = this.deeltjes.filter(
      deeltje => !deeltje.isDood()
    );
  }
}
```

<iframe loading="lazy" src="simulatie1/03.html" width="720px" height="430px" frameBorder="0"></iframe>

## School vissen { id=vissen }

Demonstratie van het samenstromen van een school vissen.
Sleep met de muis om vissen aan het systeem toe te voegen.

<small markdown="1">Algoritme door Craig Reynolds zie <http://www.red3d.com/cwr/></small>

<small markdown="1">Origineel door Daniel Shiffman, zie <http://natureofcode.com></small>

```javascript
let school;

function setup() {
  createCanvas(640, 360);
  // Maak honderd vissen om te starten
  school = Array.from({ length: 100 },
    () => new Vis(random() * width, random() * height)
  );
}

function draw() {
  background('darkslateblue');
  school.forEach((vis) => vis.zwem(school));
}

// Voeg een nieuwe vis toe
function mouseDragged() {
  school.push(new Vis(mouseX, mouseY));
}

// TODO: ?? samenhang ≠ synoniem cohesie ??

// Vis class, met methodes voor cohesie, scheiding en uitlijning
class Vis {
  constructor(x, y) {
    this.versnelling = createVector(0, 0);
    this.snelheid = createVector(random(-1, 1), random(-1, 1));
    this.positie = createVector(x, y);
    this.grootte = 8.0;
    this.maxSnelheid = 3;  // Maximale snelheid
    this.maxKracht = 0.05; // Maximale stuur kracht
  }

  zwem(school) {
    this.stroomSamen(school);
    this.vernieuw();
    this.randen();
    this.verschijn();
  }

  zetKracht(kracht) {
    // Hier zouden we massa kunnen toevoegen als we willen
    // versnelling = kracht / massa
    this.versnelling.add(kracht);
  }

  // We herberekenen de versnelling steeds p basis van drie regels
  stroomSamen(school) {
    let scheiding  = this.scheidt(school);   // scheiding
    let uitlijning = this.lijnUit(school);   // uitlijning
    let samenhang  = this.samenhang(school); // samenhang
    // We vermenigvuldigen deze krachten elk met een bepaalde factor
    scheiding.mult(1.5);
    uitlijning.mult(1.0);
    samenhang.mult(1.0);
    // We voegen deze vectoren toe aan versnelling
    this.zetKracht(scheiding);
    this.zetKracht(uitlijning);
    this.zetKracht(samenhang);
  }

  // Methode om de positie te herberekenen
  vernieuw() {
    // We versnellen ...
    this.snelheid.add(this.versnelling);
    // ... maar beperken de snelheid.
    this.snelheid.limit(this.maxSnelheid);
    // We voegen de snelheid toe aan de positie
    this.positie.add(this.snelheid);
    // We zetten versnelling weer op nul.
    this.versnelling.mult(0);
  }

  // Dit is een methode om in de richting van een doel te sturen.
  // De stuurkracht is de maximale snelheid in de gewenste richting
  // min de huidige snelheid.
  vind(doel) {
    // Een vector die wijst van de huidige locatie naar het doel
    let gewenst = p5.Vector.sub(doel, this.positie);
    // Maak de norm (lengte) van "gewenst" gelijk aan 1
    gewenst.normalize();
    // Maak de norm (lengte) van "gewenst" gelijk aan maxSnelheid
    gewenst.mult(this.maxSnelheid);
    // stuurkracht = gewenst - huidig
    let stuur= p5.Vector.sub(gewenst, this.snelheid);
    // Beperk de stuurkracht
    stuur.limit(this.maxKracht);
    return stuur;
  }

  verschijn() {
    // Teken een vis in de richting van snelheid
    let theta = this.snelheid.heading();
    fill(200);
    push();
    translate(this.positie.x, this.positie.y);
    rotate(theta);
    noStroke();
    beginShape();
    vertex(-this.grootte, -this.grootte/4);
    bezierVertex(
      -this.grootte/2, this.grootte/3,
       this.grootte/2, -this.grootte,
       this.grootte, 0
    );
    bezierVertex(
      this.grootte/2, this.grootte,
      -this.grootte/3, -this.grootte/3,
      -this.grootte, this.grootte/4
    );
    endShape(CLOSE);
    pop();
  }

  // Laat de vis aan de overzijde verschijnen
  // als die van het scherm gaat.
  randen() {
    if (this.positie.x < -this.grootte)
      this.positie.x = width + this.grootte;
    if (this.positie.y < -this.grootte)
      this.positie.y = height + this.grootte;
    if (this.positie.x > width + this.grootte)
      this.positie.x = -this.grootte;
    if (this.positie.y > height + this.grootte)
      this.positie.y = -this.grootte;
  }

  // Scheiding
  // stuur weg van andere vissen die vlakbij zijn
  scheidt(school) {
    let gewensteScheiding = 25.0;
    let stuur = createVector(0, 0);
    let teller = 0;
    // Controleer voor elke vis in het systeem of die te dichtbij is.
    school.forEach((vis) => {
      let afstand = p5.Vector.dist(this.positie, vis.positie);
      // Als de afstand groter is dan 0 en minder dan gewensteScheiding...
      if ((afstand > 0) && (afstand < gewensteScheiding)) {
        // ... bereken dan een vector die in de andere richting wijst
        let verschil = p5.Vector.sub(this.positie, vis.positie);
        // maak de lengte van verschil 1
        verschil.normalize();
        // maak de lengte van verschil langer wanneer de afstand korter is
        verschil.div(afstand);
        // voeg het bij de stuur vector
        stuur.add(verschil);
        // tel hoeveel vissen vlakbij zijn
        teller++;
      }
    });
    // Neem een gemiddelde door te delen door het aantal
    if (teller > 0) {
      stuur.div(teller);
    }
    // Zo lang de stuur vector langer is dan 0 ...
    if (stuur.mag() > 0) {
      // ... maak de stuur kracht = gewenst - huidig
      // maak de lengte 1
      stuur.normalize();
      // maak ze dan gelijk aan this.maxSnelheid
      // (dit is de gewenste snelheid)
      stuur.mult(this.maxSnelheid);
      // verminder met de huidige snelheid
      stuur.sub(this.snelheid);
      // beperk met maximale kracht
      stuur.limit(this.maxKracht);
    }
    return stuur;
  }

  // Uitlijning
  // Bereken van andere vissen die vlakbij zijn de gemiddelde snelheid
  lijnUit(school) {
    let maxAfstand = 50;
    let stuur = createVector(0, 0);
    let teller = 0;
    school.forEach((vis) => {
      let afstand = p5.Vector.dist(this.positie, vis.positie);
      if ((afstand > 0) && (afstand < maxAfstand)) {
        stuur.add(vis.snelheid);
        teller++;
      }
    });
    // Neem een gemiddelde door te delen door het aantal
    if (teller > 0) {
      stuur.div(teller);
    }
    // Zo lang de stuur vector langer is dan 0 ...
    if (stuur.mag() > 0) {
      stuur.div(teller);
      stuur.normalize();
      stuur.mult(this.maxSnelheid);
      stuur.sub(this.snelheid);
      stuur.limit(this.maxKracht);
      return stuur;
    } else {
      return createVector(0, 0);
    }
  }

  // Samenhang
  // Bereken de gemiddelde positie van alle omliggende vissen samen
  // en bereken een bepaalde stuurkracht naar die plek.
  samenhang(school) {
    let maxAfstand = 50;
    let stuur = createVector(0, 0);
    let teller = 0;
    school.forEach((vis) => {
      let afstand = p5.Vector.dist(this.positie, vis.positie);
      if ((afstand > 0) && (afstand < maxAfstand)) {
        stuur.add(vis.positie); // voeg locatie toe
        teller++;
      }
    });
    // Neem een gemiddelde door te delen door het aantal
    if (teller > 0) {
      stuur.div(teller);
    }
    // Zo lang de stuur vector langer is dan 0 ...
    if (stuur.mag() > 0) {
      return this.vind(stuur); // Stuur in die richting
    } else {
      return createVector(0, 0);
    }
  }
}
```

<iframe loading="lazy" src="simulatie1/05.html" width="650px" height="430px" frameBorder="0"></iframe>

## Wolfram CA { id=wolfram }

Dit is een eenvoudig voorbeeld van een cellulaire automaat met één dimensie naar het voorbeeld van Stephen Wolfram.

<small markdown="1">Origineel door Daniel Shiffman, zie <http://natureofcode.com></small>

> In 1983 en in 2002 (in zijn boek A New Kind of Science) bestudeerde Stephen Wolfram eendimensionale cellulaire automaten met twee toestanden; de zogenoemde elementaire cellulaire automaten.
> Deze kregen elk een eigen nummer aan de hand van de regels die de cellulaire automaat gebruikte.
> Deze cellulaire automaten vertoonden bepaalde eigenschappen zoals het genereren van willekeurige patronen (Regel 30) of Turingvolledigheid (Regel 110).

<small markdown="1">Overgenomen van [dit Wikipedia artikel](https://nl.wikipedia.org/w/index.php?title=Cellulaire_automaat)</small>


```javascript
let eenheid = 10;
// Dit wordt een lijstje met nullen en enen.
let vakjes;

let generatie = 0;

// Een lijstje om de regels in op te slaan,
// bijvoorbeeld 0,1,1,0,1,1,0,1
let regels = [0, 1, 0, 1, 1, 0, 1, 0];

function setup() {
  createCanvas(640, 400);
  // Maak 'vakjes' een leeg lijstje
  // van een bepaalde lengte
  vakjes = Array(floor(width / eenheid));
  // Vul het lijstje met nullen
  vakjes.fill(0);
  // We kiezen ervoor om het middelste vakje
  // de waarde één te geven.
  vakjes[vakjes.length/2] = 1;
}

function draw() {
  vakjes.forEach((vakje,index) => {
    if (vakje === 1) {
      fill(200);
    } else {
      fill(51);
      noStroke();
      rect(
        index * eenheid,
        generatie * eenheid,
        eenheid,
        eenheid
      );
    }
  });
  if (generatie < height/eenheid) {
    nieuweGeneratie();
  }
}

function nieuweGeneratie() {
  // Maak 'nieuw' en leeg lijstje van een dezelfde lengte als 'vakjes'
  let nieuw = Array(vakjes.length);
  // Bepaal voor elk vakje de nieuwe staat door de huidige staat
  // en die van de omringende vakjes aan de regels te onderwerpen.
  // Negeer de randen die maar één gebuur hebben.
  for (let i = 1; i < vakjes.length-1; i++) {
    let links   = vakjes[i-1];   // Linker buur
    let huidig  = vakjes[i];     // Huidig vakje
    let rechts  = vakjes[i+1];   // Rechter buur
    nieuw[i] = volgDeRegels(links, huidig, rechts);
  }
  // Maak van de huidige generatie de nieuwe generatie
  vakjes = nieuw;
  generatie++;
}


// Hier passen we de Wolfram regels toe
// Dit zou beter en korter kunnen geschreven worden,
// maar zo zie je expliciet wat er gaande is voor elk geval
function volgDeRegels(a, b, c) {
  if (a == 1 && b == 1 && c == 1) return regels[0];
  if (a == 1 && b == 1 && c == 0) return regels[1];
  if (a == 1 && b == 0 && c == 1) return regels[2];
  if (a == 1 && b == 0 && c == 0) return regels[3];
  if (a == 0 && b == 1 && c == 1) return regels[4];
  if (a == 0 && b == 1 && c == 0) return regels[5];
  if (a == 0 && b == 0 && c == 1) return regels[6];
  if (a == 0 && b == 0 && c == 0) return regels[7];
  return 0;
}
```

<iframe loading="lazy" src="simulatie1/07.html" width="650px" height="430px" frameBorder="0"></iframe>

## Game of Life { id=conway }

Dit voorbeeld implementeert Game of Life, een cellulaire automaat van John Conway

```javascript
let eenheid;
let kolommen;
let rijen;
let rooster;
let volgende;

function setup() {
  createCanvas(720, 400);
  eenheid = 10;
  // Bereken kolommen en rijen
  kolommen = floor(width / eenheid);
  rijen = floor(height / eenheid);
  // Een manier om een (2D) lijstje van lege lijstjes te maken
  rooster = new Array(kolommen);
  for (let i = 0; i < kolommen; i++) {
    rooster[i] = new Array(rijen);
  }
  // ... en nog eentje
  volgende = new Array(kolommen);
  for (let i = 0; i < kolommen; i++) {
    volgende[i] = new Array(rijen);
  }
  begin();
}

function draw() {
  background(0);
  nieuweGeneratie();
  for ( let i = 0; i < kolommen;i++) {
    for ( let j = 0; j < rijen;j++) {
      if ((rooster[i][j] == 1)) {
        fill(255);
      } else {
        fill(0);
      }
      stroke(50);
      rect(i * eenheid, j * eenheid, eenheid-1, eenheid-1);
    }
  }

}

// vernieuw het rooster wanneer de muis wordt geklikt
function mousePressed() {
  begin();
}

// Vul het rooster met willekeurige waarden
function begin() {
  for (let i = 0; i < kolommen; i++) {
    for (let j = 0; j < rijen; j++) {
      // Aan de randen komen nullen
      if (i == 0 || j == 0 || i == kolommen-1 || j == rijen-1) {
        rooster[i][j] = 0;
      } else {
        // De rest is willekeurig nul of één
        rooster[i][j] = floor(random(2));
      }
      volgende[i][j] = 0;
    }
  }
}

function nieuweGeneratie() {
  for (let x = 1; x < kolommen - 1; x++) {
    for (let y = 1; y < rijen - 1; y++) {
      // Tel voor elk vakje de waarden van de omringende vakjes op.
      let waarde = 0;
      for (let i = -1; i <= 1; i++) {
        for (let j = -1; j <= 1; j++) {
          waarde += rooster[x+i][y+j];
        }
      }

      // Omdat we hierboven niet enkel waarden
      // van de omringende vakjes optelden, maar er ook
      // steeds het huidige vakje hebben bijgeteld,
      // trekken we die van het totaal af.
      waarde -= rooster[x][y];

      // De regels van het leven
      // Eenzaamheid
      // Overbevolking
      // Voortplanting
      // Onveranderlijkheid
      if      ((rooster[x][y] == 1) && (waarde <  2)) volgende[x][y] = 0;
      else if ((rooster[x][y] == 1) && (waarde >  3)) volgende[x][y] = 0;
      else if ((rooster[x][y] == 0) && (waarde == 3)) volgende[x][y] = 1;
      else    volgende[x][y] = rooster[x][y];
    }
  }

  // Wissel van generatie!
  let oud = rooster;
  rooster = volgende;
  volgende = oud;
}
```

<iframe loading="lazy" src="simulatie1/09.html" width="720px" height="410px" frameBorder="0"></iframe>

## Meerdere deeltjes systemen { id=systemen }

Klik met de muis om er een uitbarsting van deeltjes te genereren.
Elke uitbarsting is één systeem met deeltjes en 'zotte deeltjes' (*ZotDeeltje* is een subclass van *Deeltje*).
Let op het gebruik van Erfelijkheid en Polymorfisme.

<small markdown="1">Origineel door Daniel Shiffman</small>

```javascript
let systemen;

function setup() {
  createCanvas(710, 400);
  systemen = [];
}

function draw() {
  background(0);
  systemen.forEach(systeem => {
    systeem.toon();
    systeem.nieuwDeeltje();
  });
  if (systemen.length == 0) {
    fill(255);
    textAlign(CENTER);
    textSize(32);
    text("Klik met de muis om er", width / 2, height / 2 - 32);
    text("deeltjes te laten uitbarsten.", width / 2, height / 2);
  }
}

function mousePressed() {
  let d = new DeeltjesSysteem(createVector(mouseX, mouseY));
  systemen.push(d);
}

// Deeltje class
class Deeltje {
  constructor(positie) {
    this.versnelling = createVector(0, 0.05);
    this.snelheid = createVector(random(-1, 1), random(-1, 0));
    this.positie = positie.copy();
    this.levensduur = 255.0;
  }
  start() {
    this.vernieuw();
    this.toon();
  }
  vernieuw() {
    this.snelheid.add(this.versnelling);
    this.positie.add(this.snelheid);
    this.levensduur -= 2;
  }
  toon() {
    stroke(200, this.levensduur);
    strokeWeight(2);
    fill(127, this.levensduur);
    ellipse(this.positie.x, this.positie.y, 12, 12);
  }
  isDood() {
    if (this.levensduur < 0) return true;
    else return false;
  }
}

// Een subclass van Deeltje
class ZotDeeltje extends Deeltje {
  constructor(oorsprong) {
    // Maak een Deeltje met de oorsprong parameter
    super(oorsprong);
    // Voeg een nieuwe eigenschap toe
    this.theta = 0.0;
  }
  // Overschrijf de vernieuw() functie geërfd van Deeltje
  vernieuw() {
    this.snelheid.add(this.versnelling);
    this.positie.add(this.snelheid);
    this.levensduur -= 2;
    this.theta += (this.snelheid.x * this.snelheid.mag()) / 10.0;
  }
  // Overschrijf de toon() functie geërfd van Deeltje
  toon() {
    stroke(200, this.levensduur);
    strokeWeight(2);
    fill(127, this.levensduur);
    ellipse(this.positie.x, this.positie.y, 12, 12);
    push();
    translate(this.positie.x, this.positie.y);
    rotate(this.theta);
    stroke(255, this.levensduur);
    line(0, 0, 25, 0);
    pop();
  }
}

class DeeltjesSysteem {
  constructor(positie) {
    this.oorsprong = positie.copy();
    this.deeltjes = [];
  }

  nieuwDeeltje() {
    if (floor(random(2)) == 0) {
      this.deeltjes.push(new Deeltje(this.oorsprong));
    }
    else {
      this.deeltjes.push(new ZotDeeltje(this.oorsprong));
    }
  }

  toon() {
    for (let deeltje of this.deeltjes) {
      deeltje.vernieuw();
      deeltje.toon();
    }

    // Array.filter() verwijdert dingen uit een lijstje
    // die niet voldoen aan de test.
    this.deeltjes = this.deeltjes.filter(
      deeltje => !deeltje.isDood()
    );
  }
}
```

<iframe loading="lazy" src="simulatie1/11.html" width="720px" height="410px" frameBorder="0"></iframe>

## Spirograaf { id=spirograaf }

Dit voorbeeld gebruikt eenvoudige transformaties om een Spirograaf achtig effect te bekomen met cirkels die aaneen hangen.

zie ook <http://en.wikipedia.org/wiki/Spirograph>

Klik met de muis om te wisselen tussen het tonen van de omtrek en de onderliggende geometrie.

<small markdown="1">door [R. Luke DuBois](http://lukedubois.com)</small>

```javascript
// Een lijstje om alle huidige hoeken op te slaan
let sines = new Array(20);
// De straal van de centrale sine
let straal;
let teller;

// hier kan je wat mee spelen om het effect te zien:
let basis = 0.005; // de snelheid van de centrale sine
let factor = 1;    // versnelling factor voor elke sine
let alpha = 50;    // doorzichtigheid systeem

let omtrek = false; // tonen we de omtrek?

function setup() {
  createCanvas(710, 400);
  background(204);

  // bereken de straal van de centrale cirkel
  straal = height / 4;
  // richt elke sine in het begin naar boven
  sines.fill(PI);
}

function draw() {
  if (!omtrek) {
    background(204);
    stroke(0);
    noFill();
  }

  push(); // begin transformaties
  // ga naar het midden van het scherm
  translate(width / 2, height / 2);

  sines.forEach((v,i) => {
    let penBreedte = 0;
    if (omtrek) {
      stroke(0, 0, 255 * (i / sines.length), alpha);
      fill(0, 0, 255, alpha / 2);
      penBreedte = 5.0 * (1.0 - i / sines.length);
    }
    //
    let sineStraal = straal / (i + 1);
    // draai volgens de hoek
    // (de huidige waarde in het sines lijstje)
    rotate(v);
    // als we de omtrek niet tonen,
    // toon de onderliggende geometrie
    if (!omtrek) ellipse(0, 0, sineStraal * 2, sineStraal * 2);
    push(); // begin transformaties bovenop de huidige
    // ga naar de rand van de sine
    translate(0, sineStraal);
    // kies een pen breedte
    if (!omtrek) strokeWeight(5);
    else strokeWeight(penBreedte);
    point(0, 0); // teken een punt
    pop(); // einde van deze transformaties
    // ga naar de positie voor de volgende sine
    translate(0, sineStraal);
    // vernieuw de hoek aan de hand van
    // de basis snelheid en de versnelling factor
    sines[i] = (v + (basis + (basis * i * factor))) % TWO_PI;
  });
  pop(); // einde van deze transformaties
}

function mousePressed() {
    omtrek = !omtrek;
    background(255);
}
```

<iframe loading="lazy" src="simulatie1/13.html" width="720px" height="410px" frameBorder="0"></iframe>

## Spiraalveer { id=veer }

Klik en sleep de balk en laat weer los om de veer in werking te zien.

```javascript
let ballast, veer;

function setup() {
  createCanvas(640, 360);
  setFrameRate(60);
  // Maak objecten ergens in het midden van het scherm
  // Het derde argument in de Veer constructor is de "rest lengte"
  veer = new Veer(width / 2, 10, 100);
  ballast = new Ballast(width / 2, 100);
}


function draw() {
  background(51);
  // Pas zwaartekracht toe
  let zwaartekracht = createVector(0, 2);
  ballast.zetKracht(zwaartekracht);

  // verbind de ballast aan de veer (dit berekent de kracht)
  veer.verbindt(ballast);
  // beperk de lengte van de veer tussen min en max
  veer.beperkLengte(ballast, 30, 200);

  ballast.vernieuw();

  // teken alles
  veer.toonLijn(ballast); // Draw a line between veer and ballast
  ballast.toon();
  veer.toon();


}

function mousePressed() {
  ballast.wanneerIkWordtGeklikt(mouseX, mouseY);
}

function mouseDragged() {
  ballast.wanneerIkWordtGesleept(mouseX, mouseY);
}

function mouseReleased() {
  ballast.stopMetSlepen();
}

// Object om een anker punt te beschrijven
// dat kan worden verbonden met een ballast
// door middel van een veer.
// Dank aan: http://www.myphysicslab.com/spring2d.html
class Veer {
  constructor(x, y, lengte) {
    this.ankerPunt = createVector(x, y);
    this.restLengte = lengte;
    this.k = 0.2;
  }
  // Bereken kracht en zet kracht op het te verbinden lichaam
  verbindt(ballast) {
    // Vector die van het anker punt naar de positie van de ballast wijst.
    let kracht = p5.Vector.sub(ballast.positie, this.ankerPunt);
    // welke afstand is er tussen het anker en de ballast
    let d = kracht.mag();
    // uitrekking is het verschil tussen de huidige afstand en de rest lengte
    let uitrekking = d - this.restLengte;

    // Bereken de kracht volgens de wet van Hooke
    // F = k * uitrekking
    kracht.normalize();
    kracht.mult(-1 * this.k * uitrekking);
    // pas de kracht toe
    ballast.zetKracht(kracht);
  }

  // Beperk de afstand tussen ballast en het anker tussen min and max
  beperkLengte(b, min, max) {
    let dir = p5.Vector.sub(b.positie, this.ankerPunt);
    let d = dir.mag();
    if (d < min) {
      dir.normalize();
      dir.mult(min);
      // geen realistische fysica: zet de locatie en voorkom beweging
      b.positie = p5.Vector.add(this.ankerPunt, dir);
      b.snelheid.mult(0);
    } else if (d > max) {
      dir.normalize();
      dir.mult(max);
      // geen realistische fysica: zet de locatie en voorkom beweging
      b.positie = p5.Vector.add(this.ankerPunt, dir);
      b.snelheid.mult(0);
    }
  }

  toon() {
    stroke(255);
    fill(127);
    strokeWeight(2);
    ellipse(this.ankerPunt.x, this.ankerPunt.y, 10);
  }

  toonLijn(b) {
    strokeWeight(2);
    stroke(255);
    line(b.positie.x, b.positie.y, this.ankerPunt.x, this.ankerPunt.y);
  }
}

class Ballast {
  constructor(x, y) {
    this.positie = createVector(x, y);
    this.snelheid = createVector();
    this.versnelling = createVector();
    this.massa = 24;
    // We bepalen hier demping om wrijving
    // en luchtweerstand te simuleren
    this.demping = 0.98;
    // Voor interactie
    this.sleepLengte = createVector();
    this.sleep = false;
  }

  vernieuw() {
    this.snelheid.add(this.versnelling);
    this.snelheid.mult(this.demping);
    this.positie.add(this.snelheid);
    this.versnelling.mult(0);
  }

  // een wet van Newton: F = M * A
  zetKracht(force) {
    let f = force.copy();
    f.div(this.massa);
    this.versnelling.add(f);
  }

  // Teken de ballast
  toon() {
    stroke(255);
    strokeWeight(2);
    fill(127);
    if (this.sleep) {
      fill(200);
    }
    ellipse(this.positie.x, this.positie.y, this.massa * 2, this.massa * 2);
  }

  wanneerIkWordtGeklikt(mx, my) {
    let d = dist(mx, my, this.positie.x, this.positie.y);
    if (d < this.massa) {
      this.sleep = true;
      this.sleepLengte.x = this.positie.x - mx;
      this.sleepLengte.y = this.positie.y - my;
    }
  }

  stopMetSlepen() {
    this.sleep = false;
  }

  wanneerIkWordtGesleept(mx, my) {
    if (this.sleep) {
      this.positie.x = mx + this.sleepLengte.x;
      this.positie.y = my + this.sleepLengte.y;
    }
  }
}
```

<iframe loading="lazy" src="simulatie1/15.html" width="720px" height="410px" frameBorder="0"></iframe>

## Zacht Lichaam { id=zacht }

Deze simulatie van een zacht lichaam gebruikt `curveVertex()` en `curveTightness()`.

<small>Origineel voorbeeld door Ira Greenberg.</small>

```javascript
let centrum, versnelling, snelheid;
let straal = 45, hoek = -90, veerKracht = 0.0009, demping = 0.98;

// hoek punten
let hoekPunten = 8;

let hoeken = Array.from({length: hoekPunten}, () => 0);
let startPunten = new Array(hoekPunten);
let punten, frequentie;

// hoe blubberig is het zachte lichaam?
const blubberigheid = 1;
let strakheid = blubberigheid;

function setup() {
  createCanvas(710, 400);
  centrum = createVector(width / 2, height / 2);
  snelheid = createVector(0, 0);
  versnelling = createVector(0, 0);
  frequentie = Array.from(
    {length: hoekPunten},
    () => random(5, 12)
  );
  punten = Array.from(
    {length: hoekPunten},
    () => createVector(0, 0)
  );
  noStroke();
}

function draw() {
  // laat de achtergrond vervagen
  // (met doorzichtigheid 100)
  fill(0, 100);
  rect(0, 0, width, height);
  tekenVorm();
  verplaatsVorm();
}

function tekenVorm() {
  // bereken start posities
  for (let i=0; i < hoekPunten; i++) {
    startPunten[i] = createVector(
        centrum.x + cos(radians(hoek)) * straal,
        centrum.y + sin(radians(hoek)) * straal
    );
    hoek += 360.0 / hoekPunten;
  };

  // teken polygoon
  curveTightness(strakheid);
  fill(255);
  beginShape();
  punten.forEach(punt => curveVertex(punt.x, punt.y));
  endShape(CLOSE);
}

function verplaatsVorm() {
  // bereken de snelheid
  if (mouseX === 0 || mouseY === 0) {
    // mouseX en mouseY hebben pas de correcte waarden
    // als de muis na het laden van de schets voor het
    // eerst verplaatst wordt.
    snelheid.x = 0;
    snelheid.y = 0;
  } else {
    snelheid.x = mouseX - centrum.x;
    snelheid.y = mouseY - centrum.y;
  }

  // pas veer kracht toe
  snelheid.mult(veerKracht);
  versnelling.add(snelheid);

  // verplaats centrum
  centrum.add(versnelling);

  // pas demping toe
  versnelling.mult(demping);

  // verander hoe strak de curve is
  strakheid = blubberigheid - ((abs(versnelling.x) + abs(versnelling.y)) * 0.1);

  // verplaats hoekpunten
  for (let i = 0; i < hoekPunten; i++){
    // hmmm... is sin() voor x bewust gekozen? probeer eens cos()
    punten[i].x = startPunten[i].x + sin(radians(hoeken[i])) * (versnelling.x * 2);
    punten[i].y = startPunten[i].y + sin(radians(hoeken[i])) * (versnelling.y * 2);
    hoeken[i] += frequentie[i];
  }
}
```

<iframe loading="lazy" src="simulatie1/17.html" width="720px" height="410px" frameBorder="0"></iframe>

## Sneeuwvlokken { id=sneeuw }

Deze simulatie van sneeuwvlokken is een systeem met gebruik van een lijstje van Javascript objecten.

<small>Origineel door Aatish Bhatia.</small>

```javascript
// In dit lijstje komen sneeuwvlok objecten
let sneeuwvlokken = [];

function setup() {
  createCanvas(500, 400);
  fill(240);
  noStroke();
}

function draw() {
  background(100);
  // tijd interval om de vlok te animeren
  let t = frameCount / 60;

  // maak een willekeurige hoeveelheid sneeuwvlokken elk frame
  for (let i = 0; i < random(5); i++) {
    // voeg een sneeuwvlok object toe aan het lijstje
    sneeuwvlokken.push(new Sneeuwvlok());
  }

  for (let vlok of sneeuwvlokken) {
    vlok.vernieuw(t); // vernieuw de positie van de vlok
    vlok.toon(); // teken de vlok
  }
}

class Sneeuwvlok {
  constructor() {
    this.posX = 0;
    this.posY = random(-50, 0);
    this.hoek = random(0, 2 * PI);
    this.grootte = random(2, 5);
    // de straal van de spiraal waarmee de vlok valt
    // zo gekozen dat alle vlokken uniform verspreid zijn
    this.straal = sqrt(random(pow(width / 2, 2)));
  }

  vernieuw(time) {
    // de x positie volgt een cirkel
    let w = 0.6; // draai snelheid
    let hoek = w * time + this.hoek;
    this.posX = width / 2 + this.straal * sin(hoek);

    // verschillende groottes van sneeuwvlokken vallen
    // met een lichtjes andere snelheid
    this.posY += pow(this.grootte, 0.5);

    // verwijder een vlokje uit het lijstje als het van het scherm is
    if (this.posY > height) {
      let index = sneeuwvlokken.indexOf(this);
      sneeuwvlokken.splice(index, 1);
    }
  };

  toon() {
    ellipse(this.posX, this.posY, this.grootte);
  };
}
```

<iframe loading="lazy" src="simulatie1/19.html" width="510px" height="410px" frameBorder="0"></iframe>

[← Wiskunde 2](wiskunde2.html) [Simulatie 2 →](simulatie2.html)
