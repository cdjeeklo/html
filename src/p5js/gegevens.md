# Gegevens

## Overzicht

<p class="overzicht r6 count" markdown="1">
[Variabelen](#variabelen)
[True en False](#bool)
[Het "bestek" van variabelen](#scope)
[Nummers](#nummers)
</p>


## Variabelen { id=variabelen }

Variabelen worden gebruikt om waarden in op te slaan.
Verander in dit voorbeeld de waarden van de variabelen om de compositie te veranderen.

```javascript
function setup() {
  createCanvas(720, 400);
  background(0);
  stroke(153);
  strokeWeight(4);
  strokeCap(SQUARE);

  let a = 50;
  let b = 120;
  let c = 180;

  line(a, b, a + c, b);
  line(a, b + 10, a + c, b + 10);
  line(a, b + 20, a + c, b + 20);
  line(a, b + 30, a + c, b + 30);

  a = a + c;
  b = height - b;

  line(a, b, a + c, b);
  line(a, b + 10, a + c, b + 10);
  line(a, b + 20, a + c, b + 20);
  line(a, b + 30, a + c, b + 30);

  a = a + c;
  b = height - b;

  line(a, b, a + c, b);
  line(a, b + 10, a + c, b + 10);
  line(a, b + 20, a + c, b + 20);
  line(a, b + 30, a + c, b + 30);
}
```

<iframe loading="lazy" src="gegevens/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## True en False { id=bool }

Een Boolean variabele heeft maar twee mogelijke waarden: true of false.
Het is gebruikelijk om Booleans te gebruiken in control statements om het verloop van het programma te bepalen.
In dit voorbeeld worden verticale lijnen getekend wanneer de waarde van "b" true is, of wanneer het false is, worden er horizontale lijnen getekend.

```javascript
function setup() {
  createCanvas(720, 400);
  background(0);
  stroke(255);

  let b = false;
  let d = 20;
  let midden = width / 2;

  for (let i = d; i <= width; i += d) {
    b = i < midden;

    if (b === true) {
      // Verticale lijn
      line(i, d, i, height - d);
    }

    if (b === false) {
      // Horizontale lijn
      line(midden, i - midden + d, width - d, i - midden + d);
    }
  }
}
```

<iframe loading="lazy" src="gegevens/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## Het *bereik* of *bestek* van variabelen { id=scope }

Variabelen hebben ofwel een globaal "bestek" ofwel een functie "bestek".
Variabelen die bijvoorbeeld in `setup()` gemaakt worden kunnen enkel in `setup()` gebruikt worden.
Dat is ook zo in een `for` lus.
Globale variabelen daarentegen, dus variabelen die daarbuiten gemaakt werden, kunnen overal in het programma gebruikt worden.
Als een variabele in een functie dezelfde naam krijgt als één die al bestaat in de in het globaal "bestek", dan zal het programma in die functie de lokale variabele.

```javascript
let a = 80; // Maak een globale variabele "a"

function setup() {
  createCanvas(720, 400);
  background(0);
  stroke(255);
  noLoop();
}

function draw() {
  // Teken een lijn aan de hand van de globale variable "a"
  line(a, 0, a, height);

  // Gebruik een lokale variable "a" in een for lus
  for (let a = 120; a < 200; a += 3) {
    line(a, 0, a, height);
  }

  // roep de zelfgemaakte functie tekenNogEenLijn() aan
  tekenNogEenLijn();

  // roep de zelfgemaakte functie tekenNogEenAndereLijn() aan
  tekenNogEenAndereLijn();
}

function tekenNogEenLijn() {
  // Maak een nieuwe variabele "a" die lokaal is in deze functie
  let a = 320;
  // Teken een lijn aan de hand van de lokale variable "a"
  line(a, 0, a, height);
}

function tekenNogEenAndereLijn() {
  // Omdat ze hier geen lokale variabele "a" hebben gemaakt,
  // is deze lijn getekend met de originele globale waarde van "a"
  line(a + 3, 0, a + 3, height);
}
```

<iframe loading="lazy" src="gegevens/05.html" width="720px" height="410px" frameBorder="0"></iframe>

## Nummers { id=nummers }

Nummers kunnen geschreven worden met of zonder getallen achter de komma.
Een "integer" is een geheel getal (een getal zonder decimaal punt).
Een "float" is een kommagetal.

```javascript
let a = 0; // Maak een globale variabele met de naam "a" van het type Number
let b = 0; // Maak een globale variabele met de naam "b" van het type Number

function setup() {
  createCanvas(720, 400);
  stroke(255);
}

function draw() {
  background(0);

  a = a + 1;   // Vermeerder "a" met één
  b = b + 0.2; // Vermeerder "b" met een kommagetal
  line(a, 0, a, height / 2);
  line(b, height / 2, b, height);

  if (a > width) {
    a = 0;
  }
  if (b > width) {
    b = 0;
  }
}
```

<iframe loading="lazy" src="gegevens/07.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Vorm](vorm.html) [Lijstjes →](lijstjes.html)
