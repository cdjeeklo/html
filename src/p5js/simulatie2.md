# Simulatie 2

## Overzicht

<p class="overzicht r4 count" markdown="1">
[Wat is een L-systeem?](#uitleg)
[eenvoudig L-systeem](#lsysteem)
[Penrose L-systeem](#penrose)
</p>

Deze voorbeelden visualiseren Lindenmayer systemen.

## Wat is een L-systeem? { id="uitleg" }

Met *L-systemen* worden lineaire en vertakte (groeiende) structuren gesimuleerd.
Ze worden veel gebruikt om gaandeweg natuurlijke, geometrische of gecompliceerde meetkundige figuren te genereren.
Ze zijn geïntroduceerd in 1968 door Aristid Lindenmayer, die er de groei van planten mee modelleerde.

### Hoe werkt het?

Een *L-systeem* is eigenlijk een algoritme om een reeks letters of tekens recursief te veranderen.
Nadien kunnen we een betekenis geven aan een letter of teken.
Zo kunnen we bijvoorbeeld bepalen hoe lijnen en vormen worden getekend.

* Eerst bepalen we welke letters of tekens we gaan gebruiken (het *alfabet*).
* Dan bepalen we de reeks (het gegeven, het *axioma*) waarmee we starten.
* Daarna schrijven we regels waarmee de tekst zal worden veranderd en bepalen we hoeveel keer we de regels toepassen.

### Eenvoudig voorbeeld

**alfabet**: AB

**axioma**: A

**regels**: A → ABA, B → BBB

Als we de regels vijf keer toepassen:

1. A
2. ABA
3. ABABBBABA
4. ABABBBABABBBBBBBBBABABBBABA
5. ABABBBABABBBBBBBBBABABBBABABBBBBBBBBBBBBBBBBBBBBBBBBBBABABBBABABBBBBBBBBABABBBABA

### Complexere regels

Als `A` gevolgd wordt door `C`, moet `A` door `-B` worden vervangen.

* A > C → -B

## Een eenvoudig L-systeem { id=lsysteem }

<small markdown="1">Door [R. Luke DuBois](http://lukedubois.com/)</small>

<small markdown="1">zie <https://en.wikipedia.org/wiki/L-system></small>

De tekens kunnen gebruikt worden als commando om iets (bijvoorbeeld een robot of een schildpad) van bovenuit gezien te laten navigeren.
Zo betekent een min teken `-` in dit voorbeeld *draai straks naar rechts*.

```javascript
// aangeven van de huidige positie en richting
let x, y;
let richting = 0;
let eenheid = 20; // hoe veel verplaatsing een stap is
let hoek = 90;    // hoe veel te draaien met '-' or '+'

let gegeven = 'A';
let herschrijfStappen = 5;
let regels = [
  ['A', '-BF+AFA+FB-'],
  ['B', '+AF-BFB-FA+']
];

let teller = 0; // tel welk teken aan de beurt is

function setup() {
  createCanvas(710, 400);
  background(0);
  stroke(150, 150, 150, 255);

  // we beginnen links onderaan
  x = 0;
  y = height-1;

  // bereken het L-systeem
  for (let i = 0; i < herschrijfStappen; i++) {
    gegeven = lindenmayer(gegeven);
  }
}

function draw() {
  // toon iets aan de hand van het huidige
  // karakter in het gegeven
  toon(gegeven[teller]);

  // vermeerder de teller, of herbegin op nul
  // als hij voorbij het laatste karakter
  // van het gegeven zou zijn
  teller++;
  if (teller > gegeven.length-1) teller = 0;
}

// interpretatie L-system
function lindenmayer(invoer) {
  let uitvoer="", zelfdeTeken=false;

  // pas elke regel toe op elke teken
  // van de gegeven invoer
  for (const teken of invoer) {
    zelfdeTeken = false;
    for (const regel of regels) {
      if (teken == regel[0]) {
        zelfdeTeken = true;
        // kopieer de tekens uit de regel
        // naar de uitvoer
        uitvoer += regel[1];
        break;
      }
    };
    // de regel kon niet toegepast worden
    // dus kopiëren we het huidige teken
    // naar de uitvoer
    if (!zelfdeTeken) uitvoer += teken;
  };
  return uitvoer;
}

// Deze functie tekent steeds vanuit
// de laatste positie en richting.
function toon(k) {
  // teken vooruit
  if (k == 'F') {
    // zet een stapje in de goede richting
    // door de berekening van cartesiaanse
    // coördinaten aan de hand van de polaire
    // parameters eenheid en richting.
    let x1 = x + eenheid*cos(radians(richting));
    let y1 = y + eenheid*sin(radians(richting));
    // lijn van de huidige (x, y) naar (x1, y1)
    line(x, y, x1, y1);

    // vernieuw de huidige positie
    x = x1;
    y = y1;
  } else if (k == '+') {
    // draai straks naar links
    richting += hoek;
  } else if (k == '-') {
    // draai straks naar rechts
    richting -= hoek;
  }

  // willekeurige kleuren
  let r = random(128, 255);
  let g = random(0, 192);
  let b = random(0, 50);
  let a = random(50, 100);

  let straal = randomGaussian(7, 5);

  // teken een cirkel
  fill(r, g, b, a);
  ellipse(x, y, straal, straal);
}
```

<iframe loading="lazy" src="simulatie2/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Penrose Rhombus L-system { id=penrose }

(er is iets mis, zie [origineel hieronder](#penroseorig))

```javascript
// aangeven van de huidige positie en richting
let eenheid;    // hoe veel verplaatsing een stap is
let pauze = false;

// Lindenmayer
let gegeven = '[X]++[X]++[X]++[X]++[X]';
let herschrijfStappen = 5;
let regels = [
  ['W', 'YF++ZF----XF[-YF----WF]++'],
  ['X', '+YF--ZF[---WF--XF]+'],
  ['Y', '-WF++XF[+++YF++ZF]-'],
  ['Z', '--YF++++WF[+ZF++++XF]--XF']
];

let teller = 0; // tel welk teken aan de beurt is

function setup() {
  createCanvas(710, 400);
  eenheid = width;
  background(0);
  noFill();
  stroke(255, 60);

  // bereken het L-systeem
  for (let i = 0; i < herschrijfStappen; i++) {
    gegeven = lindenmayer(gegeven);
    eenheid *= 0.5;
  }
}

function mousePressed(){
  if (pauze) { pauze=false; noLoop(); }
  else { pauze=true; loop(); }
}

function draw() {
  translate(width / 2, height / 2);
  while ((teller + 1) % 20 != 0) {
    // teken iets aan de hand van het huidige
    // karakter in het gegeven
    toon(gegeven[teller]);
    // vermeerder de teller
    teller++;
  }
  // vermeerder de teller
  teller++;
  // herbegin op nul als de teller
  // voorbij het laatste karakter
  // van het gegeven zou zijn
  if (teller > gegeven.length-1) {
    teller = 0;
    eenheid = width;
    background(0);
  }
}

// interpretatie L-system
function lindenmayer(invoer) {
  let uitvoer="", zelfdeTeken=false;

  // pas elke regel toe op elke teken
  // van de gegeven invoer
  for (const teken of invoer) {
    zelfdeTeken = false;
    for (const regel of regels) {
      if (teken == regel[0]) {
        zelfdeTeken = true;
        // kopieer de tekens uit de regel
        // naar de uitvoer
        uitvoer += regel[1];
        break;
      }
    };
    // de regel kon niet toegepast worden
    // dus kopiëren we het huidige teken
    // naar de uitvoer
    if (teken != 'F' && !zelfdeTeken) uitvoer += teken;
  };
  return uitvoer;
}

// Deze functie tekent steeds vanuit
// de laatste positie en richting.
function toon(k) {
  // teken vooruit
  if (k === 'F') {
    strokeWeight(2);
    line(0, 0, 0, -eenheid);
    translate(0, -eenheid);
  } else if (k === '+') {
    // draai naar links
    rotate(TWO_PI / 10);
  } else if (k === '-') {
    // draai naar rechts
    rotate(-TWO_PI / 10);
  } else if (k === '[') {
    push();
  } else if (k === ']') {
    pop();
  }
}
```

<iframe loading="lazy" src="simulatie2/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## Penrose (origineel) { id=penroseorig }

```javascript
/*
 * @name Penrose Tiles
 * @frame 710,400
 * @description This is a port by David Blitz of the "Penrose Tile" example from processing.org/examples
 */

let ds;

function setup() {
  createCanvas(710, 400);
  ds = new PenroseLSystem();
  //please, play around with the following line
  ds.simulate(5);
}

function draw() {
  background(0);
  ds.render();
}

function PenroseLSystem() {
    this.steps = 0;

   //these are axiom and rules for the penrose rhombus l-system
   //a reference would be cool, but I couldn't find a good one
    this.axiom = "[X]++[X]++[X]++[X]++[X]";
    this.ruleW = "YF++ZF----XF[-YF----WF]++";
    this.ruleX = "+YF--ZF[---WF--XF]+";
    this.ruleY = "-WF++XF[+++YF++ZF]-";
    this.ruleZ = "--YF++++WF[+ZF++++XF]--XF";

    //please play around with the following two lines
    this.startLength = 460.0;
    this.theta = TWO_PI / 10.0; //36 degrees, try TWO_PI / 6.0, ...
    this.reset();
}

PenroseLSystem.prototype.simulate = function (gen) {
  while (this.getAge() < gen) {
    this.iterate(this.production);
  }
}

PenroseLSystem.prototype.reset = function () {
    this.production = this.axiom;
    this.drawLength = this.startLength;
    this.generations = 0;
  }

PenroseLSystem.prototype.getAge = function () {
    return this.generations;
  }

//apply substitution rules to create new iteration of production string
PenroseLSystem.prototype.iterate = function() {
    let newProduction = "";

    for(let i=0; i < this.production.length; ++i) {
      let step = this.production.charAt(i);
      //if current character is 'W', replace current character
      //by corresponding rule
      if (step == 'W') {
        newProduction = newProduction + this.ruleW;
      }
      else if (step == 'X') {
        newProduction = newProduction + this.ruleX;
      }
      else if (step == 'Y') {
        newProduction = newProduction + this.ruleY;
      }
      else if (step == 'Z') {
        newProduction = newProduction + this.ruleZ;
      }
      else {
        //drop all 'F' characters, don't touch other
        //characters (i.e. '+', '-', '[', ']'
        if (step != 'F') {
          newProduction = newProduction + step;
        }
      }
    }

    this.drawLength = this.drawLength * 0.5;
    this.generations++;
    this.production = newProduction;
}

//convert production string to a turtle graphic
PenroseLSystem.prototype.render = function () {
    translate(width / 2, height / 2);

    this.steps += 20;
    if(this.steps > this.production.length) {
      this.steps = this.production.length;
    }

    for(let i=0; i<this.steps; ++i) {
      let step = this.production.charAt(i);

      //'W', 'X', 'Y', 'Z' symbols don't actually correspond to a turtle action
      if( step == 'F') {
        stroke(255, 60);
        for(let j=0; j < this.repeats; j++) {
          line(0, 0, 0, -this.drawLength);
          noFill();
          translate(0, -this.drawLength);
        }
        this.repeats = 1;
      }
      else if (step == '+') {
        rotate(this.theta);
      }
      else if (step == '-') {
        rotate(-this.theta);
      }
      else if (step == '[') {
        push();
      }
      else if (step == ']') {
        pop();
      }
    }
  }


```

<iframe loading="lazy" src="simulatie2/05.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Simulatie 1](simulatie1.html) [Interactie →](interactie.html)
