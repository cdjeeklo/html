# Structuur

## Overzicht

<p class="overzicht r5 count" markdown="1">
[Commentaren en Statements](#statements)
[Coördinaten](#coordinaten)
[Breedte en Hoogte](#breedte)
[Setup en Draw ](#draw)
[No Loop ](#noloop)
[Redraw](#redraw)
[Functies](#functies)
[Recursie](#recursie)
[Create Graphics](#graphics)
</p>

## Commentaren en Statements { id=statements }

Statements zijn de bouwstenen waaruit programmas zijn gemaakt.
Een ";" (punt-comma) symbool is gebruikt om een statement te beëindigen.
Dit wordt de *statement terminator* genoemd.
Commentaren worden gebruikt voor notities die mensen wellicht helpen om de broncode van programma's beter te begrijpen.
Een commentaar begint met twee voorwaartse schuine streepjes (*//*).


Een functie heeft nul of meer parameters.

Parameters zijn gegevens die worden doorgegeven om in de functie te gebruiken, en worden wellicht gebruikt om instructies aan de computer te geven.

```javascript
// De createCanvas() functie is een statement die instructies aan
// de computer geeft voor hoe groot het canvas moet zijn.
function setup() {
   createCanvas(710, 400);
}

// De background() functie is een statement die instructies aan
// de computer geeft voor het vullen van de achtergrond van het
// canvas in een bepaalde kleur.
function draw() {
   background(204, 153, 0);
}
```

<iframe loading="lazy" src="structuur/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Coördinaten { id=coordinaten }

Alle vormen die op het scherm zijn getekend hebben een positie die als coördinaat wordt omschreven.
Alle coördinaten worden gemeten als de afstand van de oorsprong en in de eenheid pixels.
De oorsprong `[0, 0]` is de coördinaat in de linker bovenhoek van het venster en de coördinaat in de rechter onderhoek is `[width-1, height-1]`.

```javascript
function setup() {
  // Maakt het canvas 720 pixels breed en 400 pixels hoog.
  createCanvas(720, 400);
}

function draw() {
  // Maakt de achtergrond zwart en zet de vul kleur uit
  background(0);
  noFill();

  // De twee parameters van de point() methode zijn coördinaten.
  // De eerste parameter is de x-coördinaat en de tweede is de y
  stroke(255);
  point(width * 0.5, height * 0.5);
  point(width * 0.5, height * 0.25);

  // Coördinaten worden gebruikt om alle vormen te tekenen, niet
  // alleen punten.  Parameters voor verschillende functies
  // worden gebruikt voor verschillende doeleinden. Bijvoorbeeld:
  // de eerste twee parameters in line() specificeren de
  // coördinaten voor het eerste uiterste en de tweede voor het
  // andere uiterste.
  stroke(0, 153, 255);
  line(0, height * 0.33, width, height * 0.33);

  // De eerste twee parameters in rect() zijn de coördinaten van
  // de linker bovenhoek en de derde en vierde parameter is voor
  // de breedte en de hoogte.
  stroke(255, 153, 0);
  rect(width * 0.25, height * 0.1, width * 0.5, height * 0.8);
}
```

<iframe loading="lazy" src="structuur/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## Breedte en Hoogte { id=breedte }


De `width` en de `height` variabelen bevatten de breedte en de hoogte van het venster waarin we iets afbeelden zoals ze werden bepaald in de `createCanvas()` functie.


```javascript
function setup() {
  createCanvas(720, 400);
}

function draw() {
  background(127);
  noStroke();
  for (let i = 0; i < height; i += 20) {
    fill(129, 206, 15);
    rect(0, i, width, 10);
    fill(255);
    rect(i, 0, 10, height);
  }
}
```

<iframe loading="lazy" src="structuur/05.html" width="720px" height="410px" frameBorder="0"></iframe>

## Setup en Draw  { id=draw }

De code in de `draw()` functie zal continu uitgevoerd worden van boven naar beneden tot het programma gestopt wordt.


```javascript
let y = 100;

// De code in de setup() functie wordt één keer uitgevoerd
// wanneer het programma begint.
function setup() {
  // createCanvas moet eerst komen
  createCanvas(720, 400);
  stroke(255); // Maak de lijn kleur wit
  frameRate(30);
}
// De code in de draw() functie wordt continu uitgevoerd tot het
// programma gestopt wordt.  Elke lijn wordt na de vorige
// uitgevoerd, en na de laatste komt de eerste weer.

function draw() {
  background(0); // Maak de achtergrond zwart
  y = y - 1;
  if (y < 0) {
    y = height;
  }
  line(0, y, width, y);
}
```

<iframe loading="lazy" src="structuur/07.html" width="720px" height="410px" frameBorder="0"></iframe>

## No Loop  { id=noloop }


De `noLoop()` functie zorgt ervoor dat `draw()` maar één keer uitgevoerd wordt.
Als `noLoop()` niet gebruikt wordt, zal de code in `draw()` de hele tijd uitgevoerd worden.


```javascript
let y;

// De statements in de setup() functie worden één keer uitgevoerd
// wanneer het programma begint
function setup() {
  // createCanvas zou het eerste statement moeten zijn
  createCanvas(720, 400);
  stroke(255); // Maak de lijn kleur wit
  noLoop();

  y = height * 0.5;
}

// De statements in draw() worden uitgevoerd tot het programma stopt.
// Elk statement is uitgevoerd in volgorde, en na de laatste lijn wordt
// de eerste weer uitgevoerd.
function draw() {
  background(0); // Maak de achtergrond zwart
  y = y - 1;
  if (y < 0) {
    y = height;
  }
  line(0, y, width, y);
}
```

<iframe loading="lazy" src="structuur/09.html" width="720px" height="410px" frameBorder="0"></iframe>

## Redraw { id=redraw }

De `redraw()` functie zorgt ervoor dat `draw()` één keer wordt uitgevoerd.
In dit voorbeeld wordt `draw()` één keer uitgevoerd wanneer de muis wordt geklikt.


```javascript
let y;

// De statements in de setup() functie worden één keer uitgevoerd
// wanneer het programma begint
function setup() {
  createCanvas(720, 400);
  stroke(255);
  noLoop();
  y = height * 0.5;
}

// De statements in draw() worden uitgevoerd tot het programma stopt.
// Elk statement is uitgevoerd in volgorde, en na de laatste lijn wordt
// de eerste weer uitgevoerd.
function draw() {
  background(0);
  y = y - 4;
  if (y < 0) {
    y = height;
  }
  line(0, y, width, y);
}

function mousePressed() {
  redraw();
}
```

<iframe loading="lazy" src="structuur/11.html" width="720px" height="410px" frameBorder="0"></iframe>

## Functies { id=functies }


De `tekenDoelwit()` functie maakt het gemakkelijk om veel verschillende doelwitten te tekenen.
Elke keer we `tekenDoelwit()` aanroepen specificeren we de positie, grootte, en aantal ringen voor elk doelwit.

```javascript
function setup() {
  createCanvas(720, 400);
  background(51);
  noStroke();
  noLoop();
}

function draw() {
  tekenDoelwit(width * 0.25, height * 0.4, 200, 4);
  tekenDoelwit(width * 0.5, height * 0.5, 300, 10);
  tekenDoelwit(width * 0.75, height * 0.3, 120, 6);
}

function tekenDoelwit(xloc, yloc, size, num) {
  const grayvalues = 255 / num;
  const steps = size / num;
  for (let i = 0; i < num; i++) {
    fill(i * grayvalues);
    ellipse(xloc, yloc, size - i * steps, size - i * steps);
  }
}
```

<iframe loading="lazy" src="structuur/13.html" width="720px" height="410px" frameBorder="0"></iframe>

## Recursie { id=recursie }

Dit demonstreert recursie: wanneer functies zichzelf aanroepen.
Een recursieve functie moet een voorwaarde bevatten om te beëindigen, anders zal het zichzelf oneindig herhalen.
Merk op hoe `tekenCirkel()` zichzelf aanroept aan het einde. Dit gebeurt tot de variabele "level" gelijk is aan 1.


```javascript
function setup() {
  createCanvas(720, 560);
  noStroke();
  noLoop();
}

function draw() {
  tekenCirkel(width / 2, 280, 6);
}

function tekenCirkel(x, radius, level) {
  // 'level' is de variabele die een einde maakt aan de recursie
  // wanneer het een bepaalde waarde bereikt (hier 1). Als zo'n
  // voorwaarde hier niet wordt gebruikt, zal de functie zichzelf
  // keer op keer blijven aanroepen tot er geen stack space meer is.
  const tt = (126 * level) / 4.0;
  fill(tt);
  ellipse(x, height / 2, radius * 2, radius * 2);
  // voorwaarde om de functie te beëindigen
  if (level > 1) {
    // 'level' vermindert met 1 bij elke herhaling
    level--
    tekenCirkel(x - radius / 2, radius / 2, level);
    tekenCirkel(x + radius / 2, radius / 2, level);
  }
}
```

<iframe loading="lazy" src="structuur/15.html" width="730px" height="570px" frameBorder="0"></iframe>

## Create Graphics { id=graphics }

De `createGraphics()` functie maakt een nieuw p5.Renderer object.
Gebruik deze class als je iets wil tekenen dat niet op het scherm verschijnt, en in een buffer op het RAM geheugen van je computer staat.
De twee parameters bepalen de breedte en de hoogte in pixels.

```javascript
let pg;

function setup() {
  createCanvas(710, 400);
  pg = createGraphics(400, 250);
}

function draw() {
  fill(0, 12);
  rect(0, 0, width, height);
  fill(255);
  noStroke();
  ellipse(mouseX, mouseY, 60, 60);

  pg.background(51);
  pg.noFill();
  pg.stroke(255);
  pg.ellipse(mouseX - 150, mouseY - 75, 60, 60);

  // Toon de verborgen buffer op het scherm met image()
  image(pg, 150, 75);
}
```

<iframe loading="lazy" src="structuur/17.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Start](start.html) [Vorm →](vorm.html)
