# Start

## De p5.js web editor

De gemakkelijkste manier om te starten is met de [p5.js web editor](https://editor.p5js.org/).
Om het werk online op te slaan moeten we daar wel een account maken.

Onderaan in de online editor vinden we de console.
Daar kunnen we berichten vinden van de editor met details over de gevonden fouten.

## Je eigen editor

Je kan ook p5.js in een editor op je eigen computer gebruiken.
Je zal wel een speciale tekstverwerker nodig hebben om te programmeren: je kan kiezen uit een lijst op [wikipedia](http://en.wikipedia.org/wiki/Source_code_editor).

**TODO** check links + instructies Mac, Windows?

* [Download](#) dan een kopie van de p5.js programmabibliotheek.
* Kopieer het lege voorbeeld (de map *empty-example*) dat bij de download zit en geef je kopie een naam.
* Start een plaatselijke server in de gedownloade map
    * als je met Visual Studio Code werkt kan je [deze plugin](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) gebruiken.
    * als je python hebt, gebruik dan deze commando's in de opdrachtregel `cmd.exe`:<br>
      `cd "C:\Gebruiker\jouw-naam\desktop\Downloads\p5js"`<br>
      `python -m http.server 5500`
* Ga dan in de browser naar [http://127.0.0.1:5500/jouw-kopie-van-empty-example](#).

## p5.min.js

Als je kijkt naar index.html, zal je merken dat het naar het bestand `p5.js` verwijst.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/latest/p5.js"></script>
```

Verander de link naar `p5.min.js` als je graag de "geminificeerde" versie gebruikt.

Die versie is gecomprimeerd om sneller te laden, en toont geen extra *vriendelijke* boodschappen in de web editor console.
Je kan deze *vriendelijke* boodschappen ook uitzetten door dit bovenaan in `sketch.js` te zetten: `p5.disableFriendlyErrors = true;`.

De `<script>` tags die in `<body>` staan, worden pas geladen als de pagina geladen is.
Het is belangrijk dat de browser de programmabibliotheek `p5.js` eerst (in `<head>`) laadt, en later pas het bestand `sketch.js` (dit moet dus in `<body>`).

## Je eerste tekening

Eén van de moeilijkste dingen wanneer je begint met programmeren is dat je grammaticaal zeer nauwkeurig moet zijn.
De browser weet niet altijd wat je bedoelt, en kan nogal moeilijk doen over de plaats van leestekens.

(... TODO)

[Terug](./) [Structuur →](structuur.html)
