# Interactie

## Overzicht

## Kittelen! { id=kittelen }

Het woord "kittel" verschrikt wanneer je er de cursor over houdt.
Soms kan je het van het scherm kittelen.

```javascript
let tekst = 'kittel mij', font, afmetingen, letterGrootte = 60, x, y;

function preload() {
  font = loadFont('../../Fontin.otf');
}

function setup() {
  createCanvas(710, 400);

  textFont(font);
  textSize(letterGrootte);

  // zoek uit wat de afmetingen van de tekst zijn zodat we het kunnen centreren
  afmetingen = font.textBounds(tekst, 0, 0, letterGrootte);
  x = width / 2 - afmetingen.w / 2;
  y = height / 2 - afmetingen.h / 2;
}

function draw() {
  background(204, 120);

  // toon de tekst in het zwart en vernieuw de afmetingen
  fill(0);
  text(tekst, x, y);
  afmetingen = font.textBounds(tekst, x, y, letterGrootte);

  // ga na of de muis in het kader komt waarin de tekst is
  // en doe de tekst bewegen alsof hij gekitteld wordt.
  if (
    mouseX >= afmetingen.x &&
    mouseX <= afmetingen.x + afmetingen.w &&
    mouseY >= afmetingen.y &&
    mouseY <= afmetingen.y + afmetingen.h
  ) {
    x += random(-5, 5);
    y += random(-5, 5);
  }
}
```

<iframe loading="lazy" src="interactie/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Krabbelen! { id=krabbelen }

<small markdown="1">Door [Prof WM Harris](https://www.rit.edu/directory/wmhics-w-michelle-harris)</small>

Dit voorbeeld toont hoe je, wanneer de linker muisknop wordt gebruikt of wanneer een toets wordt ingedrukt of losgelaten, en afhankelijk van de positie van de muis, de lijndikte en de kleur aanpast.
Het loslaten van een toets zal de lijn kleur willekeurig veranderen met behulp van de random functie.

```javascript
function setup() {
  createCanvas(710, 400);
  background("beige");
  // gebruik de "hue, saturation, brightness" modus
  colorMode(HSB);
}

function draw() {
  // Teken een lijn van de vorige positie van de muis naar de huidige positie.
  line(mouseX, mouseY, pmouseX, pmouseY);
}

// Doe iets als we de linker muisknop gebruiken
function mouseClicked() {
  // één parameter, een tekst, de naam van een kleur
  stroke("slateBlue");
  // lijn dikte van 0 tot 1
  strokeWeight(random());
}

// Doe iets als we een toets loslaten
function keyReleased() {
  // als we in HSB modus drie parameters in stroke() gebruiken, is dat:
  // - hue (toon), hier kiezen we willekeurig tussen 20 and 145
  // - saturation (grijswaarde), hier tussen 0 en 100
  // - brightness (intensiteit), hier tussen 80 to 100
  stroke(random(20, 145), random(100), random(80, 100));
}

// Doe iets wanneer we een karakter typen
function keyTyped() {
  // één parameter, een tekst, de naam van een kleur
  stroke("turquoise");
  // lijn dikte van 0 tot 1
  strokeWeight(random(5));
}
```

<iframe loading="lazy" src="interactie/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## Volg de muis { id=volg }

<small>Gebaseerd op code van Keith Peters.</small>

Een lijnstuk wordt geduwd en getrokken door de cursor.

```javascript
let x = 100, y = 100, hoek = 0, lengte = 50;

function setup() {
  createCanvas(710, 400);
  strokeWeight(20);
  stroke(255, 100);
}

function draw() {
  background(0);

  dx = mouseX - x;
  dy = mouseY - y;
  hoek = atan2(dy, dx);
  x = mouseX - cos(hoek) * lengte;
  y = mouseY - sin(hoek) * lengte;

  lijnstuk(x, y, hoek);
  ellipse(x, y, 20, 20);
}

function lijnstuk(x, y, a) {
  push();
  translate(x, y);
  rotate(a);
  line(0, 0, lengte, 0);
  pop();
}
```

<iframe loading="lazy" src="interactie/05.html" width="720px" height="410px" frameBorder="0"></iframe>

Een arm bestaande uit twee segmenten volgt de positie van de muis.
De relatieve hoek tussen de segmenten is berekend met `atan2()` en de positie is met `sin()` en `cos()` berekend.

```javascript
let x = [0, 0], y = [0, 0], lengte = 50;

function setup() {
  createCanvas(710, 400);
  strokeWeight(20);
  stroke(255, 100);
}

function draw() {
  background(0);
  sleepLijnstuk(0, mouseX, mouseY);
  sleepLijnstuk(1, x[0], y[0]);
}

function sleepLijnstuk(i, xin, yin) {
  const dx = xin - x[i];
  const dy = yin - y[i];
  const hoek = atan2(dy, dx);
  x[i] = xin - cos(hoek) * lengte;
  y[i] = yin - sin(hoek) * lengte;
  lijnstuk(x[i], y[i], hoek);
}

function lijnstuk(x, y, a) {
  push();
  translate(x, y);
  rotate(a);
  line(0, 0, lengte, 0);
  pop();
}
```

<iframe loading="lazy" src="interactie/07.html" width="720px" height="410px" frameBorder="0"></iframe>

We kunnen de lijn, of de arm, in meerdere segmenten verdelen.
De relatieve hoek van het ene segment tegenover het volgende is berekend met `atan()` en de positie is met sin() en cos() berekend.

```javascript
let x = [], y = [], aantalDelen = 20, lengte = 18;

for (let i = 0; i < aantalDelen; i++) {
  x[i] = 0;
  y[i] = 0;
}

function setup() {
  createCanvas(710, 400);
  strokeWeight(9);
  stroke(255, 100);
}

function draw() {
  background(0);
  sleepLijnstuk(0, mouseX, mouseY);
  for (let i = 0; i < x.length - 1; i++) {
    sleepLijnstuk(i + 1, x[i], y[i]);
  }
}

function sleepLijnstuk(i, xin, yin) {
  const dx = xin - x[i];
  const dy = yin - y[i];
  const hoek = atan2(dy, dx);
  x[i] = xin - cos(hoek) * lengte;
  y[i] = yin - sin(hoek) * lengte;
  lijnstuk(x[i], y[i], hoek);
}

function lijnstuk(x, y, a) {
  push();
  translate(x, y);
  rotate(a);
  line(0, 0, lengte, 0);
  pop();
}
```

<iframe loading="lazy" src="interactie/09.html" width="720px" height="410px" frameBorder="0"></iframe>

## Golven maker { id=golven }

<small markdown="1">Door Aatish Bhatia, geïnspireerd door [Orbiters](https://beesandbombs.tumblr.com/post/45513650541/orbiters) door Dave Whyte.</small>

Dit toont hoe golven (zoals watergolven) worden opgebouwd met oscillerende statische deeltjes.
Beweeg je muis om de golf van richting te veranderen.

```javascript
let t = 0; // tijd variabele

function setup() {
  createCanvas(710, 400);
  noStroke();
  fill(40, 200, 40);
}

function draw() {
  // doorzichtige achtergrond (om een spoor na te laten)
  background(10, 10);

  // maak een rooster gevuld met cirkels
  for (let x = 0; x <= width; x = x + 30) {
    for (let y = 0; y <= height; y = y + 30) {
      // het startpunt van elke cirkel hangt af van de positie van de muis
      const xHoek = map(mouseX, 0, width, -4 * PI, 4 * PI, true);
      const yHoek = map(mouseY, 0, height, -4 * PI, 4 * PI, true);
      // and also varies based on the particle's location
      const hoek = xHoek * (x / width) + yHoek * (y / height);

      // elk deeltje beweegt in een cirkel
      const deeltjeX = x + 20 * cos(2 * PI * t + hoek);
      const deeltjeY = y + 20 * sin(2 * PI * t + hoek);

      ellipse(deeltjeX, deeltjeY, 10); // teken deeltje
    }
  }

  t = t + 0.01; // vermeerder tijd
}
```

<iframe loading="lazy" src="interactie/11.html" width="720px" height="410px" frameBorder="0"></iframe>

## Reik tot de muis { id=reiken }

De arm volgt de positie van de muis door de hoeken te berekenen met `atan2()`.

```javascript
let lengte = 80, x, y, x2, y2;

function setup() {
  createCanvas(710, 400);
  strokeWeight(20);
  stroke(255, 100);

  x = width / 2;
  y = height / 2;
  x2 = x;
  y2 = y;
}

function draw() {
  background(0);
  sleepLijnstuk(0, mouseX, mouseY);
  for (let i = 0; i < x.length - 1; i++) {
    sleepLijnstuk(i + 1, x[i], y[i]);
  }
}

function sleepLijnstuk(i, xin, yin) {
  background(0);

  dx = mouseX - x;
  dy = mouseY - y;
  hoek1 = atan2(dy, dx);

  tx = mouseX - cos(hoek1) * lengte;
  ty = mouseY - sin(hoek1) * lengte;
  dx = tx - x2;
  dy = ty - y2;
  hoek2 = atan2(dy, dx);
  x = x2 + cos(hoek2) * lengte;
  y = y2 + sin(hoek2) * lengte;

  lijnstuk(x, y, hoek1);
  lijnstuk(x2, y2, hoek2);
}

function lijnstuk(x, y, a) {
  push();
  translate(x, y);
  rotate(a);
  line(0, 0, lengte, 0);
  pop();
}
```

<iframe loading="lazy" src="interactie/13.html" width="720px" height="410px" frameBorder="0"></iframe>

Ook hier kunnen we een arm met meer segmenten maken:

```javascript
let aantalDelen = 10, x = [], y = [], hoek = [],
  lengte = 26, doelX, doelY;

for (let i = 0; i < aantalDelen; i++) {
  x[i] = 0;
  y[i] = 0;
  hoek[i] = 0;
}

function setup() {
  createCanvas(710, 400);
  strokeWeight(20);
  stroke(255, 100);

  x[x.length - 1] = width / 2;
  y[x.length - 1] = height;
}

function draw() {
  background(0);

  reikTotLijnstuk(0, mouseX, mouseY);
  for (let i = 1; i < aantalDelen; i++) {
    reikTotLijnstuk(i, doelX, doelY);
  }
  for (let j = x.length - 1; j >= 1; j--) {
    zetPositie(j, j - 1);
  }
  for (let k = 0; k < x.length; k++) {
    lijnstuk(x[k], y[k], hoek[k], (k + 1) * 2);
  }
}

function zetPositie(a, b) {
  x[b] = x[a] + cos(hoek[a]) * lengte;
  y[b] = y[a] + sin(hoek[a]) * lengte;
}

function reikTotLijnstuk(i, xin, yin) {
  const dx = xin - x[i];
  const dy = yin - y[i];
  hoek[i] = atan2(dy, dx);
  doelX = xin - cos(hoek[i]) * lengte;
  doelY = yin - sin(hoek[i]) * lengte;
}

function lijnstuk(x, y, a, sw) {
  strokeWeight(sw);
  push();
  translate(x, y);
  rotate(a);
  line(0, 0, lengte, 0);
  pop();
}
```

<iframe loading="lazy" src="interactie/15.html" width="720px" height="410px" frameBorder="0"></iframe>

Hier volgt de arm een bewegende bal:

```javascript
let aantalDelen = 8, x = [], y = [], hoek = [], lengte = 26,
  doelX, doelY, balX = 50, balY = 50,
  balXRichting = 1, balYRichting = -1;

for (let i = 0; i < aantalDelen; i++) {
  x[i] = 0;
  y[i] = 0;
  hoek[i] = 0;
}

function setup() {
  createCanvas(710, 400);
  strokeWeight(20);
  stroke(255, 100);
  noFill();

  x[x.length - 1] = width / 2;
  y[x.length - 1] = height;
}

function draw() {
  background(0);

  strokeWeight(20);
  balX = balX + 1.0 * balXRichting;
  balY = balY + 0.8 * balYRichting;
  if (balX > width - 25 || balX < 25) {
    balXRichting *= -1;
  }
  if (balY > height - 25 || balY < 25) {
    balYRichting *= -1;
  }
  ellipse(balX, balY, 30, 30);

  reikTotLijnstuk(0, balX, balY);
  for (let i = 1; i < aantalDelen; i++) {
    reikTotLijnstuk(i, doelX, doelY);
  }
  for (let j = x.length - 1; j >= 1; j--) {
    positionSegment(j, j - 1);
  }
  for (let k = 0; k < x.length; k++) {
    lijnstuk(x[k], y[k], hoek[k], (k + 1) * 2);
  }
}

function positionSegment(a, b) {
  x[b] = x[a] + cos(hoek[a]) * lengte;
  y[b] = y[a] + sin(hoek[a]) * lengte;
}

function reikTotLijnstuk(i, xin, yin) {
  const dx = xin - x[i];
  const dy = yin - y[i];
  hoek[i] = atan2(dy, dx);
  doelX = xin - cos(hoek[i]) * lengte;
  doelY = yin - sin(hoek[i]) * lengte;
}

function lijnstuk(x, y, a, sw) {
  strokeWeight(sw);
  push();
  translate(x, y);
  rotate(a);
  line(0, 0, lengte, 0);
  pop();
}
```

<iframe loading="lazy" src="interactie/17.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Simulatie 2](simulatie2.html) [Objecten →](objecten.html)

