# Lijstjes

Een array is een lijstje van dingen.
De positie van elk ding in een lijstje wordt met een index nummer geïdentificeerd.

**Waarden in lijstjes beginnen op de positie (index) nul.**

## Overzicht

<p class="overzicht r4 count" markdown="1">
[Cosinus](#cosinus)
[Lijstje van lijstjes](#2d)
[Array met objecten](#objecten)
</p>

## Cosinus { id=cosinus }

In dit voorbeeld is een lijstje met de naam "cosinus" gemaakt en wordt het gevuld met cosinus waarden.
Deze gegevens worden op drie verschillende manieren op het scherm getoond.

```javascript
let cosinus = [];

function setup() {
  createCanvas(720, 360);
  for (let i = 0; i < width; i++) {
    let hoeveelheid = map(i, 0, width, 0, PI);
    cosinus[i] = abs(cos(hoeveelheid));
  }
  background(255);
  noLoop();
}

function draw() {
  let y1 = 0;
  let y2 = height / 3;
  for (let i = 0; i < width; i += 3) {
    stroke(cosinus[i] * 255);
    line(i, y1, i, y2);
  }

  y1 = y2;
  y2 = y1 + y1;
  for (let i = 0; i < width; i += 3) {
    stroke((cosinus[i] * 255) / 4);
    line(i, y1, i, y2);
  }

  y1 = y2;
  y2 = height;
  for (let i = 0; i < width; i += 3) {
    stroke(255 - cosinus[i] * 255);
    line(i, y1, i, y2);
  }
}
```

<iframe loading="lazy" src="lijstjes/01.html" width="720px" height="370px" frameBorder="0"></iframe>

## Lijstje van lijstjes { id=2d }

Hier demonstreren we een manier om een lijstje te maken met 2 dimensies: een lijstje van lijstjes dus.
De waarden in een 2D lijstje vinden we terug met behulp van twee index waarden.
2D lijstjes zijn nuttig om afbeeldingen op te slaan.
In dit voorbeeld kleuren we elk punt aan de hand van de afstand van het midden van de afbeelding.

```javascript
let afstanden = [];
let maximaleAfstand;
let spatie;

function setup() {
  createCanvas(720, 360);
  maximaleAfstand = dist(width / 2, height / 2, width, height);
  for (let x = 0; x < width; x++) {
    afstanden[x] = []; // maak een lijstje van lijstjes
    for (let y = 0; y < height; y++) {
      let afstand = dist(width / 2, height / 2, x, y);
      afstanden[x][y] = (afstand / maximaleAfstand) * 255;
    }
  }
  spatie = 10;
  noLoop(); // voer draw() één keer uit en stop
}

function draw() {
  background(0);
  // Deze dubbele lus slaat waarden over aan de hand van de spatie variabele.
  // We gebruiken dus niet alle waarden in het lijstje om punten te tekenen.
  // Verander de spatie variabele om de punten dichter of verder van elkaar te brengen.
  for (let x = 0; x < width; x += spatie) {
    for (let y = 0; y < height; y += spatie) {
      stroke(afstanden[x][y]);
      point(x + spatie / 2, y + spatie / 2);
    }
  }
}
```

<iframe loading="lazy" src="lijstjes/03.html" width="720px" height="370px" frameBorder="0"></iframe>

## Array met objecten { id=objecten }

Hier demonstreren we hoe we een lijstje van eigen objecten maken.

```javascript
class Ding {
  constructor(xAfstand, yAfstand, x, y, snelheid, eenheid) {
    this.xAfstand = xAfstand;
    this.yAfstand = yAfstand;
    this.x = x;
    this.y = y;
    this.snelheid = snelheid;
    this.eenheid = eenheid;
    this.xRichting = 1;
    this.yRichting = 1;
  }

  vernieuw() {
    this.x = this.x + this.snelheid * this.xRichting;
    if (this.x >= this.eenheid || this.x <= 0) {
      this.xRichting *= -1;
      this.x = this.x + 1 * this.xRichting;
      this.y = this.y + 1 * this.yRichting;
    }
    if (this.y >= this.eenheid || this.y <= 0) {
      this.yRichting *= -1;
      this.y = this.y + 1 * this.yRichting;
    }
  }

  teken() {
    fill(255);
    ellipse(this.xAfstand + this.x, this.yAfstand + this.y, 6, 6);
  }
} // einde class Ding

// variabelen met een globaal bestek (scope)
let eenheid = 40;
let stappen;
let dingen = [];

function setup() {
  createCanvas(720, 360);
  noStroke();
  let breedteStap = width / eenheid; // lokaal bestek
  let hoogteStap = height / eenheid; // lokaal bestek
  stappen = breedteStap * hoogteStap;

  let index = 0; // lokaal bestek
  for (let y = 0; y < hoogteStap; y++) {
    for (let x = 0; x < breedteStap; x++) {
      dingen[index++] = new Ding(
        x * eenheid,
        y * eenheid,
        eenheid / 2,
        eenheid / 2,
        random(0.05, 0.8),
        eenheid
      );
    }
  }
}

function draw() {
  background(0);
  for (let i = 0; i < stappen; i++) {
    dingen[i].vernieuw();
    dingen[i].teken();
  }
}
```

<iframe loading="lazy" src="lijstjes/05.html" width="720px" height="370px" frameBorder="0"></iframe>

[← Gegevens](gegevens.html) [Controle →](controle.html)
