# Wiskunde 1

## Overzicht

<p class="overzicht r6 count" markdown="1">
[Vermeerderen](#incr)
[Bewerkingen](#bewerkingen)
[1D Afstand](#1d)
[2D Afstand](#2d)
[Sinus](#sinus)
[Sinus Cosinus](#sincos)
[Sinus golf](#golf)
[Complexe golf](#complex)
[Polair Cartesiaans](#polcart)
[Boogtangens](#atan)
[Lineaire Interpolatie](#lerp)
</p>

## Vermeerderen en verminderen { id=incr }

Het schrijven van `a++` is gelijk aan `a = a + 1`.

Het schrijven van `a--` is gelijk aan `a = a - 1`.

```javascript
let a;
let b;
let richting;

function setup() {
  createCanvas(710, 400);
  colorMode(RGB, width);
  a = 0;
  b = width;
  richting = true;
  frameRate(30);
}

function draw() {
  a++;
  if (a > width) {
    a = 0;
    richting = !richting;
  }
  if (richting === true) {
    stroke(a);
  } else {
    stroke(width - a);
  }
  line(a, 0, a, height / 2);

  b--;
  if (b < 0) {
    b = width;
  }
  if (richting == true) {
    stroke(width - b);
  } else {
    stroke(b);
  }
  line(b, height / 2 + 1, b, height);
}
```

<iframe loading="lazy" src="wiskunde1/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Volgorde van bewerkingen { id=bewerkingen }

Als je de volgorde waarin een expressie wordt geëvalueerd niet expliciet bepaalt, wordt een expressie op basis van een bepaalde volgorde van bewerkingen geëvalueerd.
In de expressie `4+2*8` wordt `2` eerst vermenigvuldigd met `8`, waarna het resultaat bij `4` wort geteld.
Dat komt omdat `*` voorrang krijgt op `+`.
Om ambiguïteit te voorkomen bij het lezen van het programma kan je er haakjes rond  plaatsen: `4+(2*8)`.
De volgorde van evaluatie kan door het plaatsen van die haakjes worden veranderd.

Hier is een lijst van bewerkingen die telkens voorrang krijgen op wat eronder staat.

* Vermenigvuldigen of delen  `*`, `/` en `%`
* Optellen of aftrekken  `+`, `-`
* Relationeel  `<`,  `>`,  `<=` en `>=`
* Gelijkheid  `==` en `!=`
* Logische AND  `&&`
* Logische OR  `||`
* Toewijzing  `=`, `+=`, `-=`, `*=`, `/=` en `%=`


```javascript
function setup() {
  createCanvas(710, 400);
  background(51);
  noFill();
  stroke(51);

  stroke(204);
  for (let i = 0; i < width - 20; i += 4) {
    // De 30 is bij de 70 gevoegd en dan geëvalueerd
    // als het groter is dan de huidige waarde van "i"
    // Voor meer duidelijkheid kan je dit schrijven
    // als "if (i > (30 + 70)) {"
    if (i > 30 + 70) {
      line(i, 0, i, 50);
    }
  }

  stroke(255);
  // De 2 is vermenigvuldigd met 8
  // en het resultaat is dan bij 4 gevoegd
  // Voor meer duidelijkheid kan je dit schrijven
  // als "rect(5 + (2 * 8), 0, 90, 20);"
  rect(4 + 2 * 8, 52, 290, 48);
  rect((4 + 2) * 8, 100, 290, 49);

  stroke(153);
  for (let i = 0; i < width; i += 2) {
    // De relationele statements zijn eerst geëvalueerd,
    // dan de logische AND statements en dan de logische OR.
    // Je kan dit ook schrijven als "if(((i > 20) && (i < 50))
    // || ((i > 100) && (i < width-20))) {"
    if ((i > 20 && i < 50) || (i > 100 && i < width - 20)) {
      line(i, 151, i, height - 1);
    }
  }
}
```

<iframe loading="lazy" src="wiskunde1/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## 1D Afstand { id=1d }

Beweeg de muis links en rechts om de snelheid en de richting van de bewegende vormen te veranderen.

```javascript
let xpos1;
let xpos2;
let xpos3;
let xpos4;
let dun = 8;
let dik = 36;

function setup() {
  createCanvas(710, 400);
  noStroke();
  xpos1 = width / 2;
  xpos2 = width / 2;
  xpos3 = width / 2;
  xpos4 = width / 2;
}

function draw() {
  background(0);

  let mx = mouseX * 0.4 - width / 5.0;

  fill(102);
  rect(xpos2, 0, dik, height / 2);
  fill(204);
  rect(xpos1, 0, dun, height / 2);
  fill(102);
  rect(xpos4, height / 2, dik, height / 2);
  fill(204);
  rect(xpos3, height / 2, dun, height / 2);

  xpos1 += mx / 16;
  xpos2 += mx / 64;
  xpos3 -= mx / 16;
  xpos4 -= mx / 64;

  if (xpos1 < -dun) {
    xpos1 = width;
  }
  if (xpos1 > width) {
    xpos1 = -dun;
  }
  if (xpos2 < -dik) {
    xpos2 = width;
  }
  if (xpos2 > width) {
    xpos2 = -dik;
  }
  if (xpos3 < -dun) {
    xpos3 = width;
  }
  if (xpos3 > width) {
    xpos3 = -dun;
  }
  if (xpos4 < -dik) {
    xpos4 = width;
  }
  if (xpos4 > width) {
    xpos4 = -dik;
  }
}
```

<iframe loading="lazy" src="wiskunde1/05.html" width="720px" height="410px" frameBorder="0"></iframe>

## 2D Afstand { id=2d }

Beweeg de muis om de cirkels in het rooster te tonen of te verbergen.
Aan de hand van de afstand van de muis tot elke cirkel wordt de grootte proportioneel bepaald.

```javascript
let maximaleAfstand;

function setup() {
  createCanvas(710, 400);
  noStroke();
  maximaleAfstand = dist(0, 0, width, height);
}

function draw() {
  background(0);

  for (let i = 0; i <= width; i += 20) {
    for (let j = 0; j <= height; j += 20) {
      let grootte = dist(mouseX, mouseY, i, j);
      grootte = (grootte / maximaleAfstand) * 66;
      ellipse(i, j, grootte, grootte);
    }
  }
}
```

<iframe loading="lazy" src="wiskunde1/07.html" width="720px" height="410px" frameBorder="0"></iframe>

## Sinus { id=sinus }

Dit past met een vlotte beweging de grootte van cirkels aan dankzij de `sin()` functie.

```javascript
let diam;
let hoek = 0;

function setup() {
  createCanvas(710, 400);
  diam = height - 10;
  noStroke();
  fill(255, 204, 0);
}

function draw() {
  background(0);

  let d1 = 10 + (sin(hoek) * diam) / 2 + diam / 2;
  let d2 = 10 + (sin(hoek + PI / 2) * diam) / 2 + diam / 2;
  let d3 = 10 + (sin(hoek + PI) * diam) / 2 + diam / 2;

  ellipse(0, height / 2, d1, d1);
  ellipse(width / 2, height / 2, d2, d2);
  ellipse(width, height / 2, d3, d3);

  hoek += 0.02;
}
```

<iframe loading="lazy" src="wiskunde1/09.html" width="720px" height="410px" frameBorder="0"></iframe>

## Sinus Cosinus { id=sincos }

Deze lineaire beweging wordt met behulp van sin() en cos() gemaakt.

Nummers tussen 0 en 2π (2π is ongeveer 6.28) worden als parameters gegeven en nummers tussen -1 en 1 krijgen we als uitkomst.
Die waarden vergroten we vervolgens om de beweging van de cirkels groter te maken.

```javascript
let hoek1 = 0;
let hoek2 = 0;
let getal = 70;

function setup() {
  createCanvas(710, 400);
  noStroke();
  rectMode(CENTER);
}

function draw() {
  background(0);

  let radialen1 = radians(hoek1);
  let radialen2 = radians(hoek2);

  let x1 = width / 2 + getal * cos(radialen1);
  let x2 = width / 2 + getal * cos(radialen2);

  let y1 = height / 2 + getal * sin(radialen1);
  let y2 = height / 2 + getal * sin(radialen2);

  fill(255);
  rect(width * 0.5, height * 0.5, 140, 140);

  fill(0, 102, 153);
  ellipse(x1, height * 0.5 - 120, getal, getal);
  ellipse(x2, height * 0.5 + 120, getal, getal);

  fill(255, 204, 0);
  ellipse(width * 0.5 - 120, y1, getal, getal);
  ellipse(width * 0.5 + 120, y2, getal, getal);

  hoek1 += 2;
  hoek2 += 3;
}
```

<iframe loading="lazy" src="wiskunde1/11.html" width="720px" height="410px" frameBorder="0"></iframe>

## Sinus golf { id=golf }

Dit voorbeeld toont een eenvoudige sinus golf.

<small>Origineel door Daniel Shiffman.</small>

```javascript
let xspatie = 16;     // Afstand tussen elke horizontale locatie
let breedte;          // Breedte van de volledige golf
let theta = 0.0;      // Start hoek op 0
let amplitude = 75.0; // Hoogte van de golf
let periode = 500.0;  // Aantal pixels tot de golf zich herhaalt
let dx;               // Waarde om x te vermeerderen
let ywaarden;         // Lijstje voor de hoogtes van de golf

function setup() {
  createCanvas(710, 400);
  breedte = width + 16;
  dx = (TWO_PI / periode) * xspatie;
  ywaarden = new Array(floor(breedte / xspatie));
}

function draw() {
  background(0);
  berekenGolf();
  toonGolf();
}

function berekenGolf() {
  // Vermeerder theta (probeer hier verschillende waarden
  // voor 'draaisnelheid')
  theta += 0.02;

  // Voor elke x waarde, bereken een y waarde
  // met de sin() functie
  let x = theta;
  for (let i = 0; i < ywaarden.length; i++) {
    ywaarden[i] = sin(x) * amplitude;
    x += dx;
  }
}

function toonGolf() {
  noStroke();
  fill(255);
  // Een eenvoudige manier om de golf te tekenen
  // met een cirkel op elke locatie
  for (let x = 0; x < ywaarden.length; x++) {
    ellipse(x * xspatie, height / 2 + ywaarden[x], 16, 16);
  }
}
```

<iframe loading="lazy" src="wiskunde1/13.html" width="720px" height="410px" frameBorder="0"></iframe>

## Complexe golf { id=complex }

Dit voorbeeld maakt een complexe golf door golven samen te voegen

<small>Origineel door Daniel Shiffman</small>

```javascript
let xspatie = 8; // Afstand tussen elke horizontale positie
let breedte;     // Breedte van de volledige golf
let aantal = 4;  // Totaal aantal golven om samen te voegen

let theta = 0.0;
// Lijstje van waarden voor de hoogte van elke golf
let amplitude = new Array(aantal);
// Lijstje van waarden waarmee we X vermeerderen voor elke golf,
// wordt in setup() berekend aan de hand van xspatie en periode.
let dx = new Array(aantal);
// Lijstje om de hoogte waarden voor één golf in op te slaan
let ywaarden;

function setup() {
  createCanvas(710, 400);
  frameRate(30);
  colorMode(RGB, 255, 255, 255, 100);
  breedte = width + 16;

  for (let i = 0; i < aantal; i++) {
    amplitude[i] = random(10, 30);
    // De periode is het aantal pixels voor de golf herhaalt
    let period = random(100, 300);
    dx[i] = (TWO_PI / period) * xspatie;
  }

  ywaarden = new Array(floor(breedte / xspatie));
}

function draw() {
  background(0);
  berekenGolf();
  toonGolf();
}

function berekenGolf() {
  // Vermeerder theta (probeer hier verschillende waarden
  // voor 'draaisnelheid')
  theta += 0.02;

  // Maak alle hoogte waarden nul
  for (let i = 0; i < ywaarden.length; i++) {
    ywaarden[i] = 0;
  }

  // Tel alle hoogte waarden van de golven op
  for (let j = 0; j < aantal; j++) {
    let x = theta;
    for (let i = 0; i < ywaarden.length; i++) {
      // Laten we afwisselen tussen een sinus golf
      // en een cosinus golf.
      // Als j deelbaar is door 2, gebruik de sin() functie,
      // anders de cos() functie
      if (j % 2 == 0) ywaarden[i] += sin(x) * amplitude[j];
      else ywaarden[i] += cos(x) * amplitude[j];
      x += dx[j];
    }
  }
}

function toonGolf() {
  // Een eenvoudige manier om de golf te tekenen
  // met een cirkel op elke locatie
  noStroke();
  fill(255, 50);
  ellipseMode(CENTER);
  for (let x = 0; x < ywaarden.length; x++) {
    ellipse(x * xspatie, width / 2 + ywaarden[x], 16, 16);
  }
}
```

<iframe loading="lazy" src="wiskunde1/15.html" width="720px" height="410px" frameBorder="0"></iframe>

## Polaire naar Cartesiaanse coördinaten { id=polcart }

Zet een polaire coördinaat `(r,θ)` om in een cartesiaanse `(x,y)`: `x = r cos(θ), y = r sin(θ)`

<small>Origineel door Daniel Shiffman.</small>

```javascript
let straal;

// Hoek, draaisnelheid en draai versnelling
let theta;
let theta_snelheid;
let theta_versnelling;

function setup() {
  createCanvas(710, 400);

  straal = height * 0.45;
  theta = 0;
  theta_snelheid = 0;
  theta_versnelling = 0.0001;
}

function draw() {
  background(0);

  // Verplaats de oorsprong naar het midden van het scherm
  translate(width / 2, height / 2);

  // Zet een polaire coördinaat om in een cartesiaanse
  let x = straal * cos(theta);
  let y = straal * sin(theta);

  // Teken een cirkel met het midden
  // op de cartesiaanse coördinaat
  ellipseMode(CENTER);
  noStroke();
  fill(200);
  ellipse(x, y, 32, 32);

  // Verander de hoek aan de hand van
  // de versnellende snelheid
  // (straal blijft gelijk in dit voorbeeld)
  theta_snelheid += theta_versnelling;
  theta += theta_snelheid;
}
```

<iframe loading="lazy" src="wiskunde1/17.html" width="720px" height="410px" frameBorder="0"></iframe>

## Boogtangens { id=atan }

Beweeg de muis om de richting van de ogen te veranderen.
De `atan2()` functie berekent de hoek van elk oog tot de cursor.

```javascript
let oog1, oog2, oog3;

function setup() {
  createCanvas(710, 400);
  noStroke();
  oog1 = new Oog(250, 16, 120);
  oog2 = new Oog(164, 185, 80);
  oog3 = new Oog(420, 230, 220);
}

function draw() {
  background(102);
  oog1.vernieuw(mouseX, mouseY);
  oog2.vernieuw(mouseX, mouseY);
  oog3.vernieuw(mouseX, mouseY);
  oog1.toon();
  oog2.toon();
  oog3.toon();
}

class Oog {
  constructor(tx, ty, ts) {
    this.x = tx;
    this.y = ty;
    this.grootte = ts;
    this.hoek = 0;
  }

  vernieuw = function(mx, my) {
    this.hoek = atan2(my - this.y, mx - this.x);
  };

  toon = function() {
    push();
    translate(this.x, this.y);
    fill(255);
    ellipse(0, 0, this.grootte, this.grootte);
    rotate(this.hoek);
    fill(153, 204, 0);
    ellipse(
      this.grootte / 4,
      0,
      this.grootte / 2,
      this.grootte / 2
    );
    pop();
  };
}
```

<iframe loading="lazy" src="wiskunde1/19.html" width="720px" height="410px" frameBorder="0"></iframe>

## Lineaire Interpolatie { id=lerp }

Beweeg de muis over het scherm en zie hoe het symbool volgt.

Hier gebruiken we de `lerp()` functie om de cirkel steeds vijf percent (`0.05`) van zijn huidige locatie in de richting van de cursor te verplaatsen.

De `lerp()` functie berekent een getal tussen twee getallen aan de hand van een bepaalde factor.
Die factor bepaalt hoe ver tussen de twee waarden dat de uitkomst moet zijn.
Als de factor nul is, is de uitkomst gelijk aan het eerste getal, en als die één is, is de uitkomst het tweede getal.

```javascript
let x = 0;
let y = 0;

function setup() {
  createCanvas(710, 400);
  noStroke();
}

function draw() {
  background(51);

  // Hier bekomen we de coördinaat
  // die 5% verder in de richting ligt
  // van de positie van de muis.
  x = lerp(x, mouseX, 0.05);
  y = lerp(y, mouseY, 0.05);

  fill(255);
  stroke(255);
  ellipse(x, y, 66, 66);
}
```

<iframe loading="lazy" src="wiskunde1/21.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Kleur](kleur.html) [Wiskunde 2 →](wiskunde2.html)
