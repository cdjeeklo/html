# Wiskunde 2

## Overzicht

<p class="overzicht r6 count" markdown="1">
[Dubbele Random](#2random)
[Random](#random)
[1D ruis](#1druis)
[Ruis golf](#noisewave)
[2D ruis](#2druis)
[3D ruis](#3druis)
[Willekeurige Koorden](#koorde)
[randomGaussian()](#gauss)
[map()](#map)
[Visualiseer een vgl](#visualiseer)
[Parametrische fn](#theta)
</p>

## Dubbele Random { id=2random }

Hier creëren we een onregelmatige lijn in de vorm van een zaagtand door twee keer `random()` te gebruiken en allemaal puntjes naast elkaar te tekenen met `point()`.

<small>Origineel door Ira Greenberg.</small>

```javascript
let stappen = 300;

function setup() {
  createCanvas(710, 400);
  stroke(255);
  frameRate(10);
}

function draw() {
  background(0);
  let rand = 0;
  for (let i = 1; i < stappen; i++) {
    point(
      (width / stappen) * i,
      height / 2 + random(-rand, rand)
    );
    rand += random(-5, 5);
  }
}
```

<iframe loading="lazy" src="wiskunde2/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Random { id=random }

Willekeurige nummers liggen aan de basis van deze afbeelding.
Elke keer dat het programma wordt geladen is het resultaat anders.

```javascript
function setup() {
  createCanvas(710, 400);
  background(0);
  strokeWeight(20);
  frameRate(2);
}

function draw() {
  for (let i = 0; i < width; i++) {
    let r = random(255);
    stroke(r);
    line(i, 0, i, height);
  }
}
```

<iframe loading="lazy" src="wiskunde2/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## 1 dimensionaal ruis { id=1druis }

Hier wordt de positie van de cirkel op het scherm bepaald door één dimensionaal Perlin Noise (graduele ruis).

```javascript
let xAfstand = 0.0;
let xStap = 0.01;

function setup() {
  createCanvas(710, 400);
  background(0);
  noStroke();
}

function draw() {
  // Maak een rechthoek zo groot als het canvas
  // en vul het met doorschijnend zwart.
  fill(0, 10);
  rect(0, 0, width, height);

  // Probeer dit eens in plaats van noise()
  //let n = random(0,width);

  // Bekom een getal tussen nul en één met noise()
  // aan de hand van xAfstand en vergroot het
  // tot de breedte van het canvas.
  let n = noise(xAfstand) * width;

  // Vermeerder xAfstand
  xAfstand += xStap;

  // Teken de cirkel op de positie waar
  // de x parameter het getal uit noise() is.
  fill(200);
  ellipse(n, height / 2, 64, 64);
}
```

<iframe loading="lazy" src="wiskunde2/05.html" width="720px" height="410px" frameBorder="0"></iframe>

## Ruis golf { id=noisewave }

Dit genereert een golfachtig patroon aan de hand van Perlin Noise.

<small>Origineel door Daniel Shiffman.</small>

```javascript
let yAfstand = 0.0;
// 2de dimensie van Perlin noise

function setup() {
  createCanvas(710, 400);
}

function draw() {
  background(51);
  fill(255);

  // We maken een vorm (polygoon) aan de hand van rechte lijnen
  beginShape();
  let xAfstand = 0; // Optie #1: 2D ruis
  // let xAfstand = yAfstand; // Optie #2: 1D ruis
  // Ga in een lus over horizontale pixels (10 per keer)
  for (let x = 0; x <= width; x += 10) {
    // Bereken een y waarde volgens noise()
    // en vermeerder en vergroot het met map()
    // zodat het tussen 200 en 300 is

    // Optie #1: 2D ruis
    let y = map(noise(xAfstand, yAfstand), 0, 1, 200, 300);

    // Option #2: 1D ruis
    // let y = map(noise(xAfstand), 0, 1, 200,300);

    // Maak een lijn als deel van de polygoon
    vertex(x, y);
    // Vermeerder de x afstand in de collectie ruis getallen
    xAfstand += 0.05;
  }
  // Vermeerder de y afstand in de collectie ruis getallen
  yAfstand += 0.01;
  // Maak twee lijnen om de vorm op de juiste plek te sluiten
  vertex(width, height);
  vertex(0, height);
  endShape(CLOSE);
}
```


<iframe loading="lazy" src="wiskunde2/07.html" width="720px" height="410px" frameBorder="0"></iframe>

## 2 dimensionaal ruis { id=2druis }

Maak 2D ruis met verschillende parameters in `noiceDetail()` om te bepalen hoe de ruis eruit ziet.


```javascript
let waarde, oktaven=5, afname=0.5;
let grootte = 0.02;

function setup() {
  createCanvas(640, 320);
  background(0);
  textSize(12);
  fill(255, 255, 255);
  // Kies een willekeurige waarde
  // (resp. niet gelijk aan 5 of 0.5)
  while (oktaven === 5 || afname === 0.5) {
    oktaven = floor(random(2, 8));
    afname = floor(random(2, 8)) / 10;
  }
  // Toon tekst met uitleg
  text(
    oktaven + ' oktaven en afname van ' + afname,
    6,
    height - 6
  );
  text(
    (10 - oktaven) + ' oktaven en afname van ' + ( 1 - afname),
    width/2 + 6,
    height - 6
  );
  strokeWeight(6);
  // Teken de linkerhelft van de afbeelding
  for (let y = 0; y < height - 20; y+=4) {
    for (let x = 0; x < width / 2; x+=4) {
      // minder fijne details die wel iets duidelijker zijn
      noiseDetail(oktaven, afname);
      waarde = noise(x * grootte, y * grootte);
      stroke(waarde * 255);
      point(x, y);
    }
  }
  // Teken de rechterhelft van de afbeelding
  for (let y = 0; y < height - 20; y+=4) {
    for (let x = width / 2; x < width; x+=4) {
      // hier met fijnere, minder duidelijke details
      // Kies het omgekeerde
      noiseDetail(10 - oktaven, 1 - afname);
      waarde = noise(x * grootte, y * grootte);
      stroke(waarde * 255);
      point(x, y);
    }
  }
}
```

<iframe loading="lazy" src="wiskunde2/09.html" width="640px" height="330px" frameBorder="0"></iframe>

## 3 dimensionaal ruis { id=3druis }

De geanimeerde textuur in dit voorbeeld gebruikt 3D ruis.

```javascript
let ruisWaarde;
// Vermeerder x met 0.01
let xStap = 0.01;
// Vermeerder z met 0.02 bij elke herhaling van draw()
let zStap = 0.02;

// Afstand waarden
let zAfstand, yAfstand, xAfstand;

toonTekst=true;

function setup() {
  createCanvas(360, 200);
  frameRate(10);
  zAfstand = 0;
  strokeWeight(8);
  // Pas de kenmerken van de ruis aan
  noiseDetail(6, 0.65);
  noLoop();
}

function mousePressed() {
  strokeWeight(8);
  toonTekst=false;
  loop();
}

function draw() {
  xAfstand = 0;
  yAfstand = 0;
  // Maak de achtergrond zwart
  background(0);

  // Bereken voor elke 5x en 5y de ruis waarde
  for (let y = 0; y < height; y+=5) {
    xAfstand += xStap;
    yAfstand = 0;

    for (let x = 0; x < width; x+=5) {
      ruisWaarde = noise(xAfstand, yAfstand, zAfstand);
      stroke(ruisWaarde * 255);
      yAfstand += xStap;
      point(x, y);
    }
  }
  zAfstand += zStap;
  if (toonTekst) {
    fill(255);
    strokeWeight(4);
    stroke(0);
    textAlign(CENTER);
    textSize(24);
    text("Dit voorbeeld belast je CPU.", width / 2, height / 2 - 32);
    text("Klik met de muis om de", width / 2, height / 2);
    text("ruis te animeren.", width / 2, height / 2 + 32);
  }
}
```

<iframe loading="lazy" src="wiskunde2/11.html" width="370px" height="210px" frameBorder="0"></iframe>

## Willekeurige Koorden { id=koorde }

In dit voorbeeld worden willekeurige koorden van een cirkel getekend.
Elke koorde is doorzichtig, waardoor ze, wanneer ze accumuleren, wellicht de illusie wekken van een bol.

<small markdown="1">Aatish Bhatia, geïnspireerd door [Anders Hoff](http://inconvergent.net/)</small>

```javascript
function setup() {
  createCanvas(400, 400);
  background(0);

  // doorzichtige lijn kleur
  stroke(200, 240, 255, 15);
}

function draw() {
  // teken twee willekeurige koorden
  koorde();
  koorde();
}

function koorde() {
  // vind een willekeurig punt op een cirkel
  let hoek1 = random(0, 2 * PI);
  let xpos1 = 200 + 200 * cos(hoek1);
  let ypos1 = 200 + 200 * sin(hoek1);

  // vind nog een willekeurig punt op een cirkel
  let hoek2 = random(0, 2 * PI);
  let xpos2 = 200 + 200 * cos(hoek2);
  let ypos2 = 200 + 200 * sin(hoek2);

  // teken er een lijn tussen
  line(xpos1, ypos1, xpos2, ypos2);
}
```

<iframe loading="lazy" src="wiskunde2/13.html" width="410px" height="410px" frameBorder="0"></iframe>

## Willekeurige nummers in een gauss curve { id=gauss }

In dit voorbeeld worden cirkels getekend met posities die van een Gaussiaanse spreiding van willekeurige nummers is afgeleid.

(oorspronkelijk van <https://processing.org/examples/randomgaussian.html>)

```javascript
function setup() {
  createCanvas(720, 400);
  background(0);
}

function draw() {

  // Bekom een nummer met randomGaussian(),
  // met een gemiddelde van width/2
  // en een standaard afwijking van 60
  let nummer = randomGaussian(width/2, 60);

  noStroke();
  fill(255, 10);
  ellipse(nummer, height/2, 32, 32);
}
```

<iframe loading="lazy" src="wiskunde2/15.html" width="720px" height="410px" frameBorder="0"></iframe>

## map() { id=map }

Gebruik de map() functie om een nummer te vergroten of te verkleinen aan de hand van een bereik.
Je kan bijvoorbeeld de x en y waarden van de positie van de muis gebruiken om de grootte of de kleur van een vorm te veranderen. In dit voorbeeld wordt de grootte en de kleur van een cirkel veranderd aan de hand van de x coördinaat van de muis.

```javascript
function setup() {
  createCanvas(720, 400);
  noStroke();
}

function draw() {
  background(0);
  // Schaal de mouseX waarde van 0 tot 720
  // naar een bereik tussen 0 en 175
  let c = map(mouseX, 0, width, 0, 175);
  // Schaal de mouseX waarde van 0 tot 720
  // naar een bereik tussen 40 en 300
  let d = map(mouseX, 0, width, 40, 300);
  fill(255, c, 0);
  ellipse(width/2, height/2, d, d);
}
```

<iframe loading="lazy" src="wiskunde2/17.html" width="720px" height="410px" frameBorder="0"></iframe>

## Visualiseer een vergelijking { id=visualiseer }

Dit voorbeeld stelt deze vergelijking grafisch voor: `sin(n cos(r) + 5θ)` waarbij n de horizontale coördinaat van de muis is.

<small>Origineel door Daniel Shiffman</small>

```javascript
let toonTekst = true;
function setup() {
  createCanvas(360, 200);
  pixelDensity(1);
  noLoop();
}

function mousePressed() {
  toonTekst=false;
  loop();
}

function draw() {
  loadPixels();
  let n = (mouseX * 10.0) / width;
  const w = 16.0; // breedte maateenheid
  const h = 16.0; // hoogte maateenheid
  const dx = w / width;  // We zullen per pixel x vermeerderen met dx
  const dy = h / height; // We zullen per pixel y vermeerderen met dy
  let x = -w / 2; // Start x op -1 * w / 2
  let y;

  let r;
  let theta;
  let waarde;

  let grijs; // variable om grijswaarde op te slaan
  let i;
  let j;
  let kolommen = width;
  let rijen = height;

  for (i = 0; i < kolommen; i += 1) {
    y = -h / 2;
    for (j = 0; j < rijen; j += 1) {
      // Bereken hoek aan de hand van cartesiaanse coördinaten
      r = sqrt(x * x + y * y);
      // Bereken de hoek aan de hand van cartesiaanse coördinaten
      theta = atan2(y, x);
      // Bereken een waarde tussen -1 en 1
      // aan de hand van de polaire coördinaten.
      waarde = sin(n * cos(r) + 5 * theta);
      //var waarde = cos(r);     // of probeer deze eenvoudige functie
      //var waarde = sin(theta); // of probeer deze eenvoudige functie
      // Gebruik de waarde als basis om de huidige pixel te kleuren
      grijs = color(((waarde + 1) * 255) / 2);
      // Vind de juiste positie in het pixels lijstje
      index = 4 * (i + j * width);
      pixels[index] = red(grijs);
      pixels[index + 1] = green(grijs);
      pixels[index + 2] = blue(grijs);
      pixels[index + 3] = alpha(grijs);

      y += dy;
    }
    x += dx;
  }
  updatePixels();
  if (toonTekst) {
    fill(255);
    strokeWeight(4);
    stroke(0);
    textAlign(CENTER);
    textSize(24);
    text("Dit voorbeeld belast je CPU.", width / 2, height / 2 - 32);
    text("Klik met de muis om de", width / 2, height / 2);
    text("vergelijking te animeren.", width / 2, height / 2 + 32);
  }
}
```

<iframe loading="lazy" src="wiskunde2/19.html" width="370px" height="210px" frameBorder="0"></iframe>


## Parametrische vergelijking { id=theta }

Een parametrische functie is een functie waarbij bijvoorbeeld x en y coördinaten samen kunnen worden uitgedrukt in een andere parameter, bijvoorbeeld de hoek theta (het symbool θ).

Inspiratie komt van het YouTube kanaal van Alexander Miller.

```javascript
function setup(){
  createCanvas(720,400);
}

// de cartesiaanse x en y kunnen
// worden afgeleid van de hoek theta
let theta = 0;
function draw(){
  background(0);
  translate(width/2,height/2);
  stroke(255);
  strokeWeight(1.5);
  // lus om 100 lijnen toe te voegen
  for(let i = 0;i<100;i++){
    line(x1(theta+i),y1(theta+i),x2(theta+i)+20,y2(theta+i)+20);
  }
  theta+=0.15;
}
// Deze functie geeft een x coördinaat
// bedoeld als startpositie van de lijn
function x1(t){
  return sin(t/10)*125+sin(t/20)*125+sin(t/30)*125;
}

// Deze functie geeft een y coördinaat
// bedoeld als startpositie van de lijn
function y1(t){
  return cos(t/10)*125+cos(t/20)*125+cos(t/30)*125;
}

// Deze functie geeft een x coördinaat
// bedoeld als eind positie van de lijn
function x2(t){
  return sin(t/15)*125+sin(t/25)*125+sin(t/35)*125;
}

// Deze functie geeft een y coördinaat
// bedoeld als eind positie van de lijn
function y2(t){
  return cos(t/15)*125+cos(t/25)*125+cos(t/35)*125;
}
```

<iframe loading="lazy" src="wiskunde2/21.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Wiskunde 1](wiskunde1.html) [Simulatie 1 →](simulatie1.html)
