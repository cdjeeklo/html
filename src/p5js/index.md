# Intro &lt;p5.js&gt;

Hallo!

De programmabibliotheek (*library*) `p5.js` stelt ons een hele reeks functies ter beschikking om te tekenen in wat we hier metaforisch een "schets" noemen.
Maar we zijn niet beperkt tot het tekenen op een canvas.
We kunnen de hele pagina beschouwen als een schets, inclusief HTML5 objecten voor tekst, invoer, video, webcam en geluid.

## Voorbeelden

Dit zijn ongeveer de voorbeelden die je kan vinden op <https://p5js.org>, maar bewerkt in hedendaags Javascript en vertaald in het Nederlands.

<p class="overzicht r8 count" markdown="1">
[Start](start.html)
[Structuur](structuur.html)
[Vorm](vorm.html)
[Gegevens](gegevens.html)
[Lijstjes](lijstjes.html)
[Controle](controle.html)
[Afbeeldingen](afbeeldingen.html)
[Kleur](kleur.html)
[Wiskunde 1](wiskunde1.html)
[Wiskunde 2](wiskunde2.html)
[Simulatie 1](simulatie1.html)
[Simulatie 2](simulatie2.html)
[Interactie](interactie.html)
[Objecten](objecten.html)
[Licht](licht.html)
[Beweging](beweging.html)
</p>

## Stappen plannen

TODO

[Overzicht](../) [structuur →](structuur.html)
