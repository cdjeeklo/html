# Kleur

## Overzicht

<p class="overzicht r4 count" markdown="1">
[Tint](#tint)
[Grijswaarde ](#grijswaarde)
[Lichtheid](#lichtheid)
[Kleur variabelen](#variabelen)
[Relativiteit](#relativiteit)
[Lineaire kleurovergang](#lineair)
[Radiale kleurovergang](#radiaal)
[lerpColor()](#lerp)
</p>


## Tint { id=tint }

Tint is de kleur die afstraalt van een object of erdoor heen schijnt, waarnaar we meestal verwijzen met een naam (rood, blauw, geel, enz.)
Beweeg de cursor verticaal over elke balk om zijn kleur te veranderen.

```javascript
const balkBreedte = 20;
let laatsteBalk = -1;

function setup() {
  createCanvas(720, 400);
  colorMode(HSB, height, height, height);
  noStroke();
  background(0);
}

function draw() {
  let welkeBalk = mouseX / balkBreedte;
  if (welkeBalk !== laatsteBalk) {
    let balkX = welkeBalk * balkBreedte;
    fill(mouseY, height, height);
    rect(balkX, 0, balkBreedte, height);
    laatsteBalk = welkeBalk;
  }
}
```

<iframe loading="lazy" src="kleur/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Grijswaarde  { id=grijswaarde }

Grijswaarde bepaalt de kracht en zuiverheid van een kleur uitgedrukt in een bepaalde hoeveelheid grijs in verhouding tot de tint.
Een "verzadigde" kleur is puur en een "onverzadigde" kleur heeft een grote hoeveelheid grijs.
Beweeg de cursor verticaal over elke balk om de verzadiging te veranderen.

```javascript
const balkBreedte = 20;
let laatsteBalk = -1;

function setup() {
  createCanvas(720, 400);
  colorMode(HSB, width, height, 100);
  noStroke();
}

function draw() {
  let welkeBalk = mouseX / balkBreedte;
  if (welkeBalk !== laatsteBalk) {
    let balkX = welkeBalk * balkBreedte;
    fill(balkX, mouseY, 66);
    rect(balkX, 0, balkBreedte, height);
    laatsteBalk = welkeBalk;
  }
}
```

<iframe loading="lazy" src="kleur/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## Lichtheid { id=lichtheid }


Dit programma past de lichtheid van een deel van de afbeelding aan door de afstand van elke pixel tot de muis te berekenen.

<small>Door Dan Shiffman.</small>

```javascript
let afbeelding;

function preload() {
  // Laad de afbeelding
  afbeelding = loadImage('../../img/coderdojo.png');
}

function setup() {
  createCanvas(720, 200);
  pixelDensity(1);
  afbeelding.loadPixels();
  loadPixels();
}

function draw() {
  for (let x = 0; x < afbeelding.width; x++) {
    for (let y = 0; y < afbeelding.height; y++) {
      // Bereken de positie aan de hand van een 2D rooster.
      // De hoeveelste pixel is dit? (links naar rechts en boven naar beneden)
      let loc = (x + y * afbeelding.width) * 4;
      // Get the R,G,B values from image
      let r, g, b;
      r = afbeelding.pixels[loc];
      // Bereken hoeveel lichtheid moet worden aangepast aan de hand van de afstand tot de muis.
      let maximaleAfstand = 50;
      let d = dist(x, y, mouseX, mouseY);
      let adjustbrightness = (255 * (maximaleAfstand - d)) / maximaleAfstand;
      r += adjustbrightness;
      // Limiteer RGB zodat de waarde van de kleur tussen 0 en 255 blijft.
      r = constrain(r, 0, 255);
      // Maak een nieuw kleur object (?)
      //color c = color(r, g, b);
      let pixloc = (y * width + x) * 4;
      pixels[pixloc] = r;
      pixels[pixloc + 1] = r;
      pixels[pixloc + 2] = r;
      pixels[pixloc + 3] = 255;
    }
  }
  updatePixels();
}
```

<iframe loading="lazy" src="kleur/05.html" width="730px" height="210px" frameBorder="0"></iframe>

## Kleur variabelen { id=variabelen }

(Hommage aan Albers.)

In dit voorbeeld worden variabelen voor kleuren gemaakt waarnaar we kunnen verwijzen in het programma.

```javascript
function setup() {
  createCanvas(710, 400);
  noStroke();
  background(51, 0, 0);

  let vanbinnen = color(204, 102, 0);
  let midden = color(204, 153, 0);
  let vanbuiten = color(153, 51, 0);

  // Deze statements zijn zoals die van hierboven.
  // Je kan zelf kiezen welk formaat je gebruikt.
  //let vanbinnen = color('#CC6600');
  //let midden = color('#CC9900');
  //let vanbuiten = color('#993300');

  push();
  translate(80, 80);
  fill(vanbuiten);
  rect(0, 0, 200, 200);
  fill(midden);
  rect(40, 60, 120, 120);
  fill(vanbinnen);
  rect(60, 90, 80, 80);
  pop();

  push();
  translate(360, 80);
  fill(vanbinnen);
  rect(0, 0, 200, 200);
  fill(vanbuiten);
  rect(40, 60, 120, 120);
  fill(midden);
  rect(60, 90, 80, 80);
  pop();
}
```

<iframe loading="lazy" src="kleur/07.html" width="720px" height="410px" frameBorder="0"></iframe>

## Relativiteit { id=relativiteit }

Elke kleur kan je hier aanschouwen in relatie met andere kleuren.
De bovenste en onderste balken bevatten elk dezelfde kleuren, maar in een andere volgorde, waardoor ze er wat anders uitzien.

```javascript
let a, b, c, d, e;

function setup() {
  createCanvas(710, 400);
  noStroke();
  a = color(165, 167, 20);
  b = color(77, 86, 59);
  c = color(42, 106, 105);
  d = color(165, 89, 20);
  e = color(146, 150, 127);
  noLoop(); // Teken maar één keer
}

function draw() {
  tekenBalk(a, b, c, d, e, 0, width / 128);
  tekenBalk(c, a, d, b, e, height / 2, width / 128);
}

function tekenBalk(v, w, x, y, z, yPositie, balkBreedte) {
  let nummer = 5;
  let kleurVolgorde = [v, w, x, y, z];
  for (let i = 0; i < width; i += balkBreedte * nummer) {
    for (let j = 0; j < nummer; j++) {
      fill(kleurVolgorde[j]);
      rect(i + j * balkBreedte, yPositie, balkBreedte, height / 2);
    }
  }
}
```

<iframe loading="lazy" src="kleur/09.html" width="720px" height="410px" frameBorder="0"></iframe>

## Lineaire kleurovergang { id=lineair }

De lerpColor() functie is nuttig om twee kleuren in elkaar te laten overgaan.

```javascript
const y_as = 1;
const x_as = 2;
let b1, b2, c1, c2;

function setup() {
  createCanvas(710, 400);

  // Definieer kleuren
  b1 = color(255);
  b2 = color(0);
  c1 = color(204, 102, 0);
  c2 = color(0, 102, 153);

  noLoop();
}

function draw() {
  // Achtergrond
  kleurovergang(0, 0, width / 2, height, b1, b2, x_as);
  kleurovergang(width / 2, 0, width / 2, height, b2, b1, x_as);
  // Voorgrond
  kleurovergang(50, 90, 540, 80, c1, c2, y_as);
  kleurovergang(50, 190, 540, 80, c2, c1, x_as);
}

function kleurovergang(x, y, breedte, hoogte, c1, c2, as) {
  noFill();

  if (as === y_as) {
    // Overgang van boven naar beneden
    for (let i = y; i <= y + hoogte; i++) {
      let tussenin = map(i, y, y + hoogte, 0, 1);
      let kleur = lerpColor(c1, c2, tussenin);
      stroke(kleur);
      line(x, i, x + breedte, i);
    }
  } else if (as === x_as) {
    // Overgang van links nar rechts
    for (let i = x; i <= x + breedte; i++) {
      let tussenin = map(i, x, x + breedte, 0, 1);
      let kleur = lerpColor(c1, c2, tussenin);
      stroke(kleur);
      line(i, y, i, y + hoogte);
    }
  }
}
```

<iframe loading="lazy" src="kleur/11.html" width="720px" height="410px" frameBorder="0"></iframe>

## Radiale kleurovergang { id=radiaal }

Dit tekent een reeks concentrische cirkels om een overgang van de ene kleur in de andere te bewerkstelligen.

```javascript
let afmeting;

function setup() {
  createCanvas(710, 400);
  afmeting = width / 2;
  background(0);
  colorMode(HSB, 360, 100, 100);
  noStroke();
  ellipseMode(RADIUS);
  frameRate(1);
}

function draw() {
  background(0);
  for (let x = 0; x <= width; x += afmeting) {
    kleurovergang(x, height / 2);
  }
}

function kleurovergang(x, y) {
  let straal = afmeting / 2;
  let kleur = random(0, 360);
  for (let r = straal; r > 0; --r) {
    fill(kleur, 90, 90);
    ellipse(x, y, r, r);
    kleur = (kleur + 1) % 360;
  }
}
```

<iframe loading="lazy" src="kleur/13.html" width="720px" height="410px" frameBorder="0"></iframe>

## lerpColor() { id=lerp }

Dit tekent willekeurige vormen en gebruikt de lerpColor() functie om kleuren tussen rood en blauw te gebruiken.

```javascript
function setup() {
  createCanvas(720, 400);
  background(255);
  noStroke();
}

function draw() {
  background(255);
  van = color(255, 0, 0, 0.2 * 255);
  tot = color(0, 0, 255, 0.2 * 255);
  c1 = lerpColor(van, tot, 0.33);
  c2 = lerpColor(van, tot, 0.66);
  fill(van);
  quad(
    random(-40, 220), random(height),
    random(-40, 220), random(height),
    random(-40, 220), random(height),
    random(-40, 220), random(height)
  );
  fill(c1);
  quad(
    random(140, 380), random(height),
    random(140, 380), random(height),
    random(140, 380), random(height),
    random(140, 380), random(height)
  );
  fill(c2);
  quad(
    random(320, 580), random(height),
    random(320, 580), random(height),
    random(320, 580), random(height),
    random(320, 580), random(height)
  );
  fill(tot);
  quad(
    random(500, 760), random(height),
    random(500, 760), random(height),
    random(500, 760), random(height),
    random(500, 760), random(height)
  );
  // vertraag de animatie tot 5 herhalingen per seconde
  frameRate(5);
}
```

<iframe loading="lazy" src="kleur/15.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Afbeeldingen](afbeeldingen.html) [Wiskunde 1 →](wiskunde1.html)
