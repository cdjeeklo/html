# Afbeeldingen

Let op! Om deze voorbeelden op je computer te testen moet je een afbeelding hebben
en een [lokale server](https://github.com/processing/p5.js/wiki/Local-server) gebruiken.

## Overzicht

<p class="overzicht r4 count" markdown="1">
[Toon een afbeelding](#toon)
</p>

## Toon een afbeelding { id=toon }

Afbeeldingen kunnen geladen en getoond worden in hun oorspronkelijke grootte of geschaald.

```javascript
let afbeelding;

function preload() {
  // Laad de afbeelding
  afbeelding = loadImage('../../img/coderdojo.png');
}

function setup() {
  createCanvas(720, 360);
  background(255);
  // Toont de afbeelding in de oorspronkelijke grootte
  // met de linker bovenhoek op (0, 0)
  image(afbeelding, 0, 0);
  // Toont de afbeelding op (40, height / 2) in halve grootte
  image(afbeelding, 40, height / 2,
    afbeelding.width / 2,
    afbeelding.height / 2
  );
}
```

<iframe loading="lazy" src="afbeeldingen/01.html" width="720px" height="370px" frameBorder="0"></iframe>


[← Controle](controle.html) [Kleur →](kleur.html)
