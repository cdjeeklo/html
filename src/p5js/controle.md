# Controle

## Overzicht

<p class="overzicht r6 count" markdown="1">
[Herhaling](#herhaling)
[Genestelde herhaling](#genesteld)
[Condities 1](#condities1)
[Condities 2](#condities2)
[Is de muis in de rechthoek?](#and)
</p>

## Herhaling { id=herhaling }

Dit voorbeeld toont herhaling met "for" lussen om patronen te tekenen.

```javascript
let y;
let num = 14;

function setup() {
  createCanvas(720, 360);
  background(102);
  noStroke();

  // Teken witte balken
  fill(255);
  y = 60;
  for (let i = 0; i < num / 3; i++) {
    rect(50, y, 475, 10);
    y += 20;
  }

  // Grijze balken
  fill(51);
  y = 40;
  for (let i = 0; i < num; i++) {
    rect(405, y, 30, 10);
    y += 20;
  }
  y = 50;
  for (let i = 0; i < num; i++) {
    rect(425, y, 30, 10);
    y += 20;
  }

  // Dunne lijnen
  y = 45;
  fill(0);
  for (let i = 0; i < num - 1; i++) {
    rect(120, y, 40, 1);
    y += 20;
  }
}
```

<iframe loading="lazy" src="controle/01.html" width="720px" height="410px" frameBorder="0"></iframe>

## Genestelde herhaling { id=genesteld }

Hier worden twee "for" lussen genesteld om herhaling in twee richtingen mogelijk te maken.

```javascript
function setup() {
  createCanvas(720, 360);
  background(0);
  noStroke();

  let roosterGrootte = 35;

  for (let x = roosterGrootte; x <= width - roosterGrootte; x += roosterGrootte) {
    for (let y = roosterGrootte; y <= height - roosterGrootte; y += roosterGrootte) {
      noStroke();
      fill(255);
      rect(x - 1, y - 1, 3, 3);
      stroke(255, 50);
      line(x, y, width / 2, height / 2);
    }
  }
}
```

<iframe loading="lazy" src="controle/03.html" width="720px" height="410px" frameBorder="0"></iframe>

## Condities 1 { id=condities2 }

Condities zijn als vragen.
Ze laten toe dat een programma een bepaalde reeks instructies uitvoert op basis van het antwoord.
Het soort vragen die in een programma gebruikt worden zijn altijd statements die vergelijkingen zijn of een boolean waarde (waar of onwaar) teruggeven.

```javascript
function setup() {
  createCanvas(720, 360);
  background(0);

  for (let i = 10; i < width; i += 10) {
    // Als 'i' deelbaar is door 20, teken dan de eerste lijn,
    // anders teken de tweede lijn.
    if (i % 20 === 0) {
      stroke(255);
      line(i, 80, i, height / 2);
    } else {
      stroke(153);
      line(i, 20, i, 180);
    }
  }
}
```

<iframe loading="lazy" src="controle/05.html" width="720px" height="410px" frameBorder="0"></iframe>

## Condities 2 { id=condities2 }

Hier voegen we aan het vorige voorbeeld vragen toe met de kernwoorden "else if".
Zo kunnen meerdere opeenvolgende vragen in de structuur gesteld worden.

```javascript
function setup() {
  createCanvas(720, 360);
  background(0);

  for (let i = 2; i < width - 2; i += 4) {
    // Als 'i' deelbaar is door 20
    if (i % 20 === 0) {
      stroke(255);
      line(i, 80, i, height / 2);
      // Als 'i' deelbaar is door 10
    } else if (i % 10 === 0) {
      stroke(153);
      line(i, 20, i, 180);
      // Als geen van de bovenstaande condities waar zijn,
      // teken dan deze lijn
    } else {
      stroke(102);
      line(i, height / 2, i, height - 20);
    }
  }
}
```

<iframe loading="lazy" src="controle/07.html" width="720px" height="410px" frameBorder="0"></iframe>

## Logische operator { id=operators }

De logische operators voor **AND** (`&&`) en **OR** (`||`) worden gebruikt om verschillende vergelijkende statements te combineren in meer complexe expressies.
De **NOT** (`!`) operator wordt gebruikt om een boolean statement om te keren.

```javascript
let test = false;

function setup() {
  createCanvas(720, 360);
  background(126);

  for (let i = 5; i <= height; i += 5) {
    // Logische AND
    stroke(0);
    if (i > 35 && i < 100) {
      line(width / 4, i, width / 2, i);
      test = false;
    }

    // Logische OR
    stroke(76);
    if (i <= 35 || i >= 100) {
      line(width / 2, i, width, i);
      test = true;
    }

    // Testen of een boolean waarde waar is
    // De expressie "if (test)" is het zelfde als "if (test == true)"
    if (test) {
      stroke(0);
      point(width / 3, i);
    }

    // Testen of een boolean waarde onwaar is
    // De expressie "if (!test)" is het zelfde als "if (test == false)"
    if (!test) {
      stroke(255);
      point(width / 4, i);
    }
  }
}
```

<iframe loading="lazy" src="controle/09.html" width="720px" height="410px" frameBorder="0"></iframe>

## Is de muis in de rechthoek? { id=and }

```javascript
function setup() {
  createCanvas(720, 360);
  background(0);
}

function draw() {
  background(0);

  // Is de muis in de rechthoek?
  // links, rechts, boven, onder
  let binnenRechthoek = (mouseX >= 150) &&
    (mouseX <= 150 + 420) &&
    (mouseY >= 100) &&
    (mouseY <= 100 + 160);

  // opvulkleur afhankelijk van de waarde van binnenRechthoek
  if (binnenRechthoek) {
    fill("pink");
  } else {
    fill("orange");
  }
  // teken de rechthoek
  rect(150, 100, 420, 160);
  // toon binnenRechthoek als tekst in de rechthoek
  fill(0);
  text("binnenRechthoek " + binnenRechthoek, 160, 120);
}
```

<iframe loading="lazy" src="controle/11.html" width="720px" height="410px" frameBorder="0"></iframe>

[← Lijstjes](lijstjes.html) [Afbeeldingen →](afbeeldingen.html)
