# Vorm

## Overzicht

<p class="overzicht r5 count" markdown="1">
[Punten en lijnen](#lijnen)
[Basis vormen](#vormen)
[Taart diagram](#diagram)
[Gewone Polygoon](#polygoon)
[Ster](#ster)
[Ring van driehoeken](#ring)
[Bezier](#bezier)
[3D Primitieven](#3d)
[Trig Wheels and Pie Chart](#wiel)
 </p>

## Punten en lijnen { id=lijnen }

Punten en lijnen kunnen gebruikt worden om eenvoudige geometrische vormen te tekenen.

Verander de waarde van de variabele `d` om de vorm te schalen.
De vier andere variabelen zetten de posities op basis van de waarde van `d`.

```javascript
function setup() {
  let d = 70;
  let p1 = d;
  let p2 = p1 + d;
  let p3 = p2 + d;
  let p4 = p3 + d;

  // Maakt het scherm 720 pixels breed en 400 pixels hoog
  createCanvas(720, 400);
  background(0);
  noSmooth();

  translate(140, 0);

  // Teken een grijze rechthoek
  stroke(153);
  line(p3, p3, p2, p3);
  line(p2, p3, p2, p2);
  line(p2, p2, p3, p2);
  line(p3, p2, p3, p3);

  // Teken witte punten
  stroke(255);
  point(p1, p1);
  point(p1, p3);
  point(p2, p4);
  point(p3, p1);
  point(p4, p2);
  point(p4, p4);
}
```

<iframe loading="lazy" src="vorm/01.html" width="730px" height="410px" frameBorder="0"></iframe>

## Basis vormen { id=vormen }

De basis vormen zijn `triangle()`, `rect()`, `quad()`, `ellipse()`, en `arc()`.
Vierkanten worden gemaakt met `rect()` en cirkels worden gemaakt met `ellipse()`.
Elk van deze functies vereisen een aantal parameters om de positie en de grootte van de vorm te bepalen.

```javascript
function setup() {
  // Maakt het scherm 720 pixels breed en 400 pixels hoog
  createCanvas(720, 400);
  background(0);
  noStroke();

  fill(204);
  triangle(18, 18, 18, 360, 81, 360);

  fill(102);
  rect(81, 81, 63, 63);

  fill(204);
  quad(189, 18, 216, 18, 216, 360, 144, 360);

  fill(255);
  ellipse(252, 144, 72, 72);

  fill(204);
  triangle(288, 18, 351, 360, 288, 360);

  fill(255);
  arc(479, 300, 280, 280, PI, TWO_PI);
}
```

<iframe loading="lazy" src="vorm/03.html" width="730px" height="410px" frameBorder="0"></iframe>

## Taart diagram { id=diagram }

Dit gebruikt de `arc()` functie om een cirkelvormig diagram te maken van de getallen in een lijstje.

```javascript
let hoeken = [30, 10, 45, 35, 60, 38, 75, 67];

function setup() {
  // Maakt het scherm 720 pixels breed en 400 pixels hoog
  createCanvas(720, 400);
  noStroke();
  noLoop(); // Voer draw() één keer uit en wacht.
}

function draw() {
  background(100);
  taartDiagram(300, hoeken);
}

function taartDiagram(diameter, gegevens) {
  let laatsteHoek = 0;
  for (let i = 0; i < gegevens.length; i++) {
    let grijswaarde = map(i, 0, gegevens.length, 0, 255);
    fill(grijswaarde);
    arc(
      width / 2,
      height / 2,
      diameter,
      diameter,
      laatsteHoek,
      laatsteHoek + radians(hoeken[i])
    );
    laatsteHoek += radians(hoeken[i]);
  }
}
```

<iframe loading="lazy" src="vorm/05.html" width="730px" height="410px" frameBorder="0"></iframe>

## Gewone Polygoon { id=polygoon }

Wat is jou favoriet? Pentagon? Hexagoon?

De `polygoon()` functie in dit voorbeeld kan elke gewone polygoon tekenen.
Probeer het eens uit met verschillende nummers als parameters.

```javascript
function setup() {
  // Maakt het scherm 720 pixels breed en 400 pixels hoog
  createCanvas(720, 400);
}

function draw() {
  background(102);

  push();
  translate(width * 0.2, height * 0.5);
  rotate(frameCount / 200.0);
  polygoon(0, 0, 82, 3);
  pop();

  push();
  translate(width * 0.5, height * 0.5);
  rotate(frameCount / 50.0);
  polygoon(0, 0, 80, 20);
  pop();

  push();
  translate(width * 0.8, height * 0.5);
  rotate(frameCount / -100.0);
  polygoon(0, 0, 70, 7);
  pop();
}

function polygoon(x, y, straal, aantal) {
  let hoek = TWO_PI / aantal;
  beginShape();
  for (let a = 0; a < TWO_PI; a += hoek) {
    let sx = x + cos(a) * straal;
    let sy = y + sin(a) * straal;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
```

<iframe loading="lazy" src="vorm/07.html" width="730px" height="410px" frameBorder="0"></iframe>

## Ster { id=ster }

De `ster()` functie in dit voorbeeld kan veel verschillende vormen tekenen.
Probeer het eens uit met verschillende nummers als parameters.


```javascript
function setup() {
  createCanvas(720, 400);
}

function draw() {
  background(102);

  push();
  translate(width * 0.2, height * 0.5);
  rotate(frameCount / 200.0);
  ster(0, 0, 5, 70, 3);
  pop();

  push();
  translate(width * 0.5, height * 0.5);
  rotate(frameCount / 50.0);
  ster(0, 0, 80, 100, 40);
  pop();

  push();
  translate(width * 0.8, height * 0.5);
  rotate(frameCount / -100.0);
  ster(0, 0, 30, 70, 5);
  pop();
}

function ster(x, y, straal1, straal2, aantal) {
  let hoek = TWO_PI / aantal;
  let halveHoek = hoek / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += hoek) {
    let sx = x + cos(a) * straal2;
    let sy = y + sin(a) * straal2;
    vertex(sx, sy);
    sx = x + cos(a + halveHoek) * straal1;
    sy = y + sin(a + halveHoek) * straal1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
```

<iframe loading="lazy" src="vorm/09.html" width="730px" height="410px" frameBorder="0"></iframe>

## Ring van driehoeken { id=ring }


Maakt een gesloten ring door gebruik te maken van de `vertex()` functie en de `beginShape(TRIANGLE_STRIP)` modus.
De variabelen `binnenStraal` en `buitenStraal` bepalen hier respectievelijk de straal van de binnen- en buitenkant.

<small>Voorbeeld door Ira Greenberg.</small>

```javascript
let x;
let y;
let buitenStraal = 150;
let binnenStraal = 100;

function setup() {
  createCanvas(720, 400);
  background(204);
  x = width / 2;
  y = height / 2;
}

function draw() {
  background(204);

  let aantalPunten = int(map(mouseX, 0, width, 6, 60));
  let hoek = 0;
  let hoekStap = 180.0 / aantalPunten;

  beginShape(TRIANGLE_STRIP);
  for (let i = 0; i <= aantalPunten; i++) {
    let px = x + cos(radians(hoek)) * buitenStraal;
    let py = y + sin(radians(hoek)) * buitenStraal;
    hoek += hoekStap;
    vertex(px, py);
    px = x + cos(radians(hoek)) * binnenStraal;
    py = y + sin(radians(hoek)) * binnenStraal;
    vertex(px, py);
    hoek += hoekStap;
  }
  endShape();
}
```

<iframe loading="lazy" src="vorm/11.html" width="730px" height="410px" frameBorder="0"></iframe>

## Bezier { id=bezier }

De twee eerste parameters voor de `bezier()` (Bézier op z'n Frans) functie bepalen het eerste punt in de curve en de laatste twee parameters bepalen het laatste punt.
De middelste parameters bepalen de vorm van de curve aan de hand van zogenoemde controle punten.

```javascript
function setup() {
  createCanvas(720, 400);
  stroke(255);
  noFill();
}

function draw() {
  background(0);
  for (let i = 0; i < 200; i += 20) {
    bezier(
      mouseX - i / 2.0,
      40 + i,
      410,
      20,
      440,
      300,
      240 - i / 16.0,
      300 + i / 8.0
    );
  }
}
```

<iframe loading="lazy" src="vorm/13.html" width="730px" height="410px" frameBorder="0"></iframe>

## 3D Primitieven { id=3d }

In een synthetische ruimte plaatsen we 3D objecten.
De `box()` en `sphere()` functies moeten met minimaal één parameter aangeroepen worden om de grootte van hun vorm te bepalen.
Deze vormen zijn verder gepositioneerd met de `translate()` functie.

```javascript
function setup() {
  createCanvas(710, 400, WEBGL);
}

function draw() {
  background(100);

  noStroke();
  fill(50);
  push();
  translate(-275, 175);
  rotateY(1.25);
  rotateX(-0.9);
  box(100);
  pop();

  noFill();
  stroke(255);
  push();
  translate(500, height * 0.35, -200);
  sphere(300);
  pop();
}
```

<iframe loading="lazy" src="vorm/15.html" width="720px" height="410px" frameBorder="0"></iframe>

## Kleurenwiel en taartdiagram { id=wiel }

Hoe maak je een kleurenwiel en hoe toon je de leeftijden van de bevolking in een taartdiagram.
Het taartdiagram bestaat uit stukken van verschillende groottes die elk een eigen kleur hebben, en het kleurenwiel bestaat uit even allemaal grote stukken die een kleurenverloop hebben.

<small>door [Prof WM Harris](https://www.rit.edu/directory/wmhics-w-michelle-harris)</small>

```javascript
function setup() {
  createCanvas(400, 400);
  colorMode(HSB);
  angleMode(DEGREES);

  // kleurenwiel middenpunt
  let x = width / 2;
  let y = height / 2 + 100;
  kleurenWiel(x, y, 100);

  noStroke();
  taartDiagram(200, 100);
}

function taartDiagram(x, y) {
  let [totaal, kind, adolescent, volwassene, zestigplus, bejaard] = [577, 103, 69,
    122, 170, 113
  ];
  let startWaarde = 0;
  let bereik = 0;

  // kind
  bereik = kind / totaal;
  tekenStuk("blue", x, y, 200, startWaarde, startWaarde + bereik);
  startWaarde += bereik;
  // adolescent
  bereik = adolescent / totaal;
  tekenStuk("orange", x, y, 200, startWaarde, startWaarde + bereik);
  startWaarde += bereik;
  // volwassene
  bereik = volwassene / totaal;
  tekenStuk("green", x, y, 200, startWaarde, startWaarde + bereik);
  startWaarde += bereik;
  // zestigplus
  bereik = zestigplus / totaal;
  tekenStuk("tan", x, y, 200, startWaarde, startWaarde + bereik);
  startWaarde += bereik;
  // bejaard
  bereik = bejaard / totaal;
  tekenStuk("pink", x, y, 200, startWaarde, startWaarde + bereik);
  startWaarde += bereik;

}

/**
 * tekenStuk - teken een gekleurde boog in een hoek op basis van percentages.
 * We passsen hier de hoeken aan zodat 0% bovenaan begint.
 * @param {color} opvulkleur
 * @param {number} x - center x
 * @param {number} y - center y
 * @param {number} d - diameter
 * @param {float} percent1 - start percentage
 * @param {float} percent2 - eind percentage
 **/


function tekenStuk(opvulkleur, x, y, d, percent1, percent2) {
  fill(opvulkleur);
  arc(x, y, d, d, -90 + percent1 * 360, -90 + percent2 * 360);
}

function kleurenWiel(x, y, rad) {
  strokeWeight(10);
  strokeCap(SQUARE);

  // Doorloop 360 graden met lijnen, in stappen van 10 graden
  for (let a = 0; a < 360; a += 10) {
    stroke(a, 150, 200); // kleur is gebaseerd op de variabale a
    // de straal is 100, het aantal graden van de hoek is in de variabele a
    line(x, y, x + rad * cos(a), y + rad * sin(a));
  }
}
```

<iframe loading="lazy" src="vorm/17.html" width="410px" height="410px" frameBorder="0"></iframe>

[← Structuur](structuur.html) [Gegevens →](gegevens.html)
