class Blokje {
  constructor(x=0,y=0){
    this.x = x;
    this.y = y;
  }
  verschijn() {
    const element = document.createElement('div');
    element.style.gridColumnStart = this.x + 1;
    element.style.gridRowStart = this.y + 1;
    element.classList.add('blokje');
    document.getElementById('spel').appendChild(element);
  }
}
export { Blokje };

