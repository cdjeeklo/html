import { Blokje } from './blokje.js'
let blokje = new Blokje();
let vorigeTijd = 0;
const SNELHEID = 10;

function lus(nu) {
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  vorigeTijd = nu;
  // hier komt wat we steeds willen uitvoeren
  blokje.verschijn();
  blokje.y++;
}
window.requestAnimationFrame(lus);
