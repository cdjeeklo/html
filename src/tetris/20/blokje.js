class Blokje {
  constructor(x=1,y=1,vorm,rotatie=0,stijl='L'){
    this.x = x || 1;
    this.y = y || 1;
    this.vorm = vorm || [
      0, 1, 0, 0,
      0, 1, 0, 0,
      0, 1, 1, 0,
      0, 0, 0, 0,
    ];
    this.r = rotatie || 0;
    this.stijl = stijl;
  }

  index(x,y) {
    switch (this.r) {
      case 0: return y * 4 + x;
      case 1: return 12 + y - (x * 4);
      case 2: return 15 - (y * 4) - x;
      case 3: return 3 - y + (x * 4);
    };
    return 0;
  }

  past(rooster,x=this.x,y=this.y) {
    let index;
    for (let bx=0; bx < 4; bx++) {
      for (let by=0; by < 4; by++) {
        if (this.vorm[this.index(bx,by)]) {
          if (rooster[(y + by) * 12 + (x + bx)] === 1) return false;
          if (x + bx + 1 < 1 || y + by + 1 < 1 || x + bx + 1 > 12 || y + by + 1 > 18) {
            return false;
          }
        }
      };
    };
    return true;
  }

  veranker(rooster) {
    for (let x=0; x < 4; x++) {
      for (let y=0; y < 4; y++) {
        if (this.vorm[this.index(x,y)]) {
          rooster[(this.y + y) * 12 + (this.x + x)] = 1;
        }
      };
    };
    // per blokje, 15 punten
    return 15;
  }

  maakLijnen(rooster) {
    let lijnen = [], gaten;
    let teControleren = this.y > 14 ? 18 - this.y : 4;
    for (let y=0; y < teControleren; y++) {
      gaten=0;
      if (this.y + y < 18) {
        for (let x=0; x < 12; x++) {
          if (!rooster[(this.y + y) * 12 + x]) {
            gaten++;
          }
        };
        if (gaten === 0) {
          lijnen.push(this.y + y);
        }
      }
      // console.log('rij', this.y + y, 'gaten', gaten)
    };
    // debugger
    if (lijnen.length > 0) {
      lijnen.forEach((lijn) => {
        for (let x=0; x < 12; x++) {
          rooster[(lijn * 12) + x] = 0;
        }
        rooster[lijn * 12] = 2;
      });
      // 1 lijn   = 1  * 20 =  20 punten
      // 2 lijnen = 4  * 20 =  80 punten
      // 3 lijnen = 8  * 20 = 160 punten
      // 4 lijnen = 16 * 20 = 320 punten
      return (lijnen.length ** 2) * 20;
    }
    return 0;
   }

  verschijn(spel) {
    for (let x=0; x < 4; x++) {
      for (let y=0; y < 4; y++) {
        if (this.vorm[this.index(x,y)]) {
          const element = document.createElement('div');
          // "+ 1" omdat css grid op één start in plaats van op nul
          element.style.gridColumnStart = this.x + x + 1;
          element.style.gridRowStart = this.y + y + 1;
          element.classList.add('blokje');
          element.classList.add(this.stijl);
          spel.appendChild(element);
        }
      }
    }
  }
}
export { Blokje };

