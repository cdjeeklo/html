import { Blokje } from './blokje.js';
import { tetrominos } from './tetrominos.js';
let nummer = Math.round(Math.random() * 6);
let blokje = new Blokje(4, 0, tetrominos[nummer], 0, 'IZSOTLJ'[nummer]);
let spel = document.getElementById('spel');
let score = document.getElementById('score');
let rooster = Array.from({ length: 18 * 12 }, () => 0);
// rooster[204] = 2;
let vorigeTijd = 0;
const SNELHEID = 10;
let teller = 0;
let punten = 0;
let naarBeneden = false;
let verloren = false;

function lus(nu) {
  if (verloren) {
    if (confirm('Verloren! Druk ok om opnieuw te spelen.')) {
      window.location.reload();
    }
    return
  }
  if (document.body.classList.contains('gepauzeerd')) return;
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  vorigeTijd = nu;
  vernieuw();
  verschijn();
}
window.requestAnimationFrame(lus);

function vernieuw() {
  teller++;
  naarBeneden = (teller === SNELHEID/2);
  if (naarBeneden) {
    teller = 0;
    if (blokje.past(rooster,blokje.x,blokje.y + 1)) {
      blokje.y++;
    } else {
      // veranker het blokje
      punten += blokje.veranker(rooster);
      // verwijder volledige lijnen
      punten += blokje.maakLijnen(rooster);
      // maak een "nieuw" blokje
      blokje.x = 4;
      blokje.y = 0;
      nummer = Math.round(Math.random() * 6);
      blokje.stijl = 'IZSOTLJ'[nummer];
      blokje.vorm = tetrominos[nummer];
      // past het blokje niet? => game over
      if (!blokje.past(rooster,blokje.x,blokje.y + 1)) {
        verloren=true;
      }
      window.clearInterval(ltimer);
      window.clearInterval(rtimer);
      window.clearInterval(btimer);
      window.clearInterval(dtimer);
    }
  }
}

function verschijn() {
  spel.innerHTML = "";
  score.innerHTML = punten;
  for (let x=0; x < 12; x++) {
    for (let y=0; y < 18; y++) {
      if (rooster[y * 12 + x]) {
        const element = document.createElement('div');
        // "+ 1" omdat css grid op één start in plaats van op nul
        element.style.gridColumnStart = x + 1;
        element.style.gridRowStart = y + 1;
        if (x === 0 && rooster[y * 12 + x] === 2) {
          element.classList.add('lijn');
        } else {
          element.classList.add('blokje');
        }
        spel.appendChild(element);
      }
    }
  }
  blokje.verschijn(spel);
  let lijn = rooster.lastIndexOf(2);
  if (lijn != -1) {
    rooster.splice(lijn,12);
    rooster.unshift(0,0,0,0,0,0,0,0,0,0,0,0);
  }
}

let links = document.getElementById('links');
let ltimer;
links.addEventListener('touchstart', e => {
  e.preventDefault();
  if (document.body.classList.contains('gepauzeerd')) {
    document.body.classList.remove('gepauzeerd')
    window.requestAnimationFrame(lus);
  }
  if (blokje.past(rooster, blokje.x - 1, blokje.y)) {
    blokje.x--;
  }
  ltimer = window.setInterval(() => {
    if (blokje.past(rooster, blokje.x - 1, blokje.y)) {
      blokje.x--;
    }
  }, 100);
});
links.addEventListener('touchend', e => {
  window.clearInterval(ltimer);
});

let rechts = document.getElementById('rechts');
let rtimer;
rechts.addEventListener('touchstart', e => {
  e.preventDefault();
  if (document.body.classList.contains('gepauzeerd')) {
    document.body.classList.remove('gepauzeerd')
    window.requestAnimationFrame(lus);
  }
  if (blokje.past(rooster, blokje.x + 1, blokje.y)) {
    blokje.x++;
  }
  rtimer = window.setInterval(() => {
    if (blokje.past(rooster, blokje.x + 1, blokje.y)) {
      blokje.x++;
    }
  }, 100);
});
rechts.addEventListener('touchend', e => {
  window.clearInterval(rtimer);
});

let beneden = document.getElementById('beneden');
let btimer;
beneden.addEventListener('touchstart', e => {
  e.preventDefault();
  if (document.body.classList.contains('gepauzeerd')) {
    document.body.classList.remove('gepauzeerd')
    window.requestAnimationFrame(lus);
  }
  if (blokje.past(rooster, blokje.x, blokje.y + 1)) {
    blokje.y++;
  };
  btimer = window.setInterval(() => {
    if (blokje.past(rooster, blokje.x, blokje.y + 1)) {
      blokje.y++;
    };
  }, 50);
});
beneden.addEventListener('touchend', e => {
  window.clearInterval(btimer);
});

let draai = document.getElementById('draai');
let dtimer;
draai.addEventListener('touchstart', e => {
  e.preventDefault();
  if (document.body.classList.contains('gepauzeerd')) {
    document.body.classList.remove('gepauzeerd')
    window.requestAnimationFrame(lus);
  }
  blokje.r < 3 ? blokje.r++ : blokje.r = 0;
  while (!blokje.past(rooster)) {
    if (blokje.x>=9) blokje.x--;
    else if (blokje.x<=2) blokje.x++;
  }
});
draai.addEventListener('touchend', e => {
  window.clearInterval(dtimer);
});

window.addEventListener('keydown', e => {
  if (document.body.classList.contains('gepauzeerd')) {
    document.body.classList.remove('gepauzeerd')
    window.requestAnimationFrame(lus);
  }
  switch (e.key) {
    case ' ':
      e.preventDefault();
      blokje.r < 3 ? blokje.r++ : blokje.r = 0;
      while (!blokje.past(rooster)) {
        if (blokje.x>=9) blokje.x--;
        else if (blokje.x<=2) blokje.x++;
      }
      break;
    case 'ArrowRight':
      e.preventDefault();
      if (blokje.past(rooster, blokje.x + 1, blokje.y)) {
        blokje.x++;
      };
      break;
    case 'ArrowDown':
      e.preventDefault();
      if (blokje.past(rooster, blokje.x, blokje.y + 1)) {
        blokje.y++;
      };
      break;
    case 'ArrowLeft':
      e.preventDefault();
      if (blokje.past(rooster, blokje.x - 1, blokje.y)) {
        blokje.x--;
      }
      break;
    case 'Tab':
    case 'Escape':
      console.log('gepauzeerd')
      document.body.classList.add('gepauzeerd')
      break;
  }
});
