class Blokje {
  constructor(x=0,y=0,vorm,rotatie=0){
    this.x = x;
    this.y = y;
    this.vorm = vorm || [
      0, 1, 0, 0,
      0, 1, 0, 0,
      0, 1, 1, 0,
      0, 0, 0, 0,
    ];
    this.r = rotatie;
  }

  index(x,y) {
    switch (this.r) {
      case 0: return y * 4 + x;
      case 1: return 12 + y - (x * 4);
      case 2: return 15 - (y * 4) - x;
      case 3: return 3 - y + (x * 4);
    };
    return 0;
  }

  verschijn() {
    for (let x=0; x < 4; x++) {
      for (let y=0; y < 4; y++) {
        if (this.vorm[this.index(x,y)]) {
          const element = document.createElement('div');
          element.style.gridColumnStart = this.x + x + 1;
          element.style.gridRowStart = this.y + y + 1;
          element.classList.add('blokje');
          document.getElementById('spel').appendChild(element);
        }
      }
    }
  }
}
export { Blokje };

