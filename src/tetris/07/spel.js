import { Blokje } from './blokje.js';
import { tetrominos } from './tetrominos.js';
let blokje = new Blokje(4, 0, tetrominos[Math.round(Math.random() * 6)]);
let spel = document.getElementById('spel');
let vorigeTijd = 0;
const SNELHEID = 10;

function lus(nu) {
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  vorigeTijd = nu;
  spel.innerHTML = "";
  blokje.verschijn(spel);
  blokje.y++;
}
window.requestAnimationFrame(lus);
