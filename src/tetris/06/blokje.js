class Blokje {
  constructor(x=0,y=0){
    this.x = x;
    this.y = y;
    this.vorm = [
      0, 1, 0, 0,
      0, 1, 0, 0,
      0, 1, 1, 0,
      0, 0, 0, 0,
    ];
  }
  verschijn() {
    for (let x=0; x < 4; x++) {
      for (let y=0; y < 4; y++) {
        if (this.vorm[y*4+x]) {
          const element = document.createElement('div');
          element.style.gridColumnStart = this.x + x + 1;
          element.style.gridRowStart = this.y + y + 1;
          element.classList.add('blokje');
          document.getElementById('spel').appendChild(element);
        }
      }
    }
  }
}
export { Blokje };

