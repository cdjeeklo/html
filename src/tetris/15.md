# Nieuwe blokjes

## Maak een *nieuw* blokje

Dit hebben we al in `spel.js`:

```javascript
let blokje = new Blokje(4, 0, tetrominos[Math.round(Math.random() * 6)]);
```

We hebben de vorm van het blokje (één van de zeven Tetromino's) willekeurig gekozen met een getal van `0` tot en met `6`.
Nu willen we een willekeurig gekozen blokje tonen in een *vooruitzicht venstertje* om het later te gebruiken in het spel.

We kunnen hiervoor een (ander) willekeurig getal van `0` tot en met `6` in de variabele `volgend` steken...

```javascript
import { Blokje } from './blokje.js';
import { tetrominos } from './tetrominos.js';
let volgend = Math.round(Math.random() * 6);
let blokje = new Blokje(4, 0, tetrominos[Math.round(Math.random() * 6)]);
```

... en later kunnen we de positie en de vorm van het blokje veranderen alsof we een *nieuw* blokje maken:

```javascript
// veranker het blokje
punten += blokje.veranker(rooster);
// maak volledige lijnen
punten += blokje.maakLijnen(rooster);
score.innerText = punten;
// maak een "nieuw" blokje
blokje.x = 4;
blokje.y = 0;
blokje.vorm = tetrominos[volgend];
```

We voegen daar nog iets aan toe.
We moeten immers het getal `volgend` veranderen!

```javascript
volgend = Math.round(Math.random() * 6);
```

Het spel werkt nu in de zin dat we verschillende blokjes kunnen stapelen.

<div class="cartoon cheerful"></div>

## Vooruitzicht venster { id=venster }

Laten we een `<div>` element met `id="venstertje"` gebruiken voor het venstertje.

```html
<body>
  <main id="spel"></main>
  <aside>
    <div id="venstertje"></div>
    <p id="score">0</p>
  </aside>
</body>
```

Het voorbeeld moet een rooster zijn van `4 x 4` ruitjes, want dat is de grootte van een blokje.
We willen dat de ruitjes even groot zijn als die in het spel rooster.

Dit hebben we al in `spel.css`:

```css
#spel {
  border: 5px #ffffff50 solid;
  box-sizing: border-box;
  width:  calc(100vh / 18 * 12);
  height: 100vh;
  display: grid;
  grid-template-rows:    repeat(18, 1fr);
  grid-template-columns: repeat(12, 1fr);
}
```

Even herhalen.
We hebben in [stap 2](02.html) de grootte van een ruitje bepaald aan de hand van de breedte van de kolommen en de hoogte van de rijen.
Die afmetingen hebben we dan weer afgeleid van de verdeling van de breedte van `#spel` in 12 gelijke delen enerzijds en een verdeling van de hoogte van `#spel` in 18 gelijke delen anderzijds.
Omdat we `#spel` zo groot mogelijk op het scherm hebben willen passen met `100vh`, moesten we de kortste kant, de breedte, aanpassen zodat er precies 12 vierkante ruitjes in zouden passen.
Vandaar de berekening `calc(100vh / 18 * 12)`: *de hoogte van de ruitjes* maal 12.

Dat kunnen we toepassen op ons `#venstertje`:

```css
#venstertje {
  border: 5px #ffffff50 solid;
  box-sizing: border-box;
  width:  calc(100vh / 18 * 4);
  height: calc(100vh / 18 * 4);
  display: grid;
  grid-template-rows:    repeat(4, 1fr);
  grid-template-columns: repeat(4, 1fr);
}
```

Laten we de `#score` ineens ook even breed maken, en `#venstertje` dezelfde zwarte achtergrond en rand geven:

```css
#score,
#venstertje {
  width: calc(100vh / 18 * 4);
  background-color: black;
  border: 5px #ffffff50 solid;
  box-sizing: border-box;
}
#score {
  font-size: 6vmin;
  text-align: right;
  padding: 1vmin 2vmin;
  margin: 0;
}
#venstertje {
  height: calc(100vh / 18 * 4);
  display: grid;
  grid-template-rows:    repeat(4, 1fr);
  grid-template-columns: repeat(4, 1fr);
}
```

Om er nu een *voorbeeld* blokje in te tonen, maken we een tweede blokje op de positie `(0, 0)` ...

```javascript
import { Blokje } from './blokje.js';
import { tetrominos } from './tetrominos.js';
let volgend = Math.round(Math.random() * 6);
let voorbeeld = new Blokje(0, 0, tetrominos[volgend]);
let blokje = new Blokje(4, 0, tetrominos[Math.round(Math.random() * 6)]);
let venstertje = document.getElementById('venstertje');
let spel = document.getElementById('spel');
```

... dat we laten verschijnen in `<div id="venstertje">` (in plaats van in `<main id="spel">`, zie [stap 7](07.html#loop)).

```javascript
let venstertje = document.getElementById('venstertje');
let spel = document.getElementById('spel');
voorbeeld.verschijn(venstertje);
```

Wanneer er in de volgende herhaling van de *game loop* een nieuw blokje zal getoond worden, maken we het venstertje leeg (dit hoeft dus enkel als het blokje verankerd wordt).
Vervolgens zetten we de vorm van het voorbeeld blokje aan de hand van het nieuwe willekeurige getal in `volgend`.
Tot slot laten we het weer `verschijn()`-en.

```javascript
// maak een "nieuw" blokje
blokje.x = 4;
blokje.y = 0;
blokje.vorm = tetrominos[volgend];
// vernieuw het willekeurig getal
volgend = Math.round(Math.random() * 6);
// maak het venstertje leeg
venstertje.innerHTML = "";
// vernieuw de vorm van het volgende blokje
voorbeeld.vorm = tetrominos[volgend];
// toon het volgende blokje
voorbeeld.verschijn(venstertje);
```

<div class="cartoon lookahead">Ik zie het volgende blokje al!</div>

We zijn bijna klaar met ons plan:

* [x] verplaats het blokje naar beneden **of** zet het blokje vast
* [x] verwijder volledige lijnen (na een kleine pauze)
* [x] vermeerder punten
* [x] kies een nieuw blokje
* [ ] stop de lus als het blokje niet past

In stap 16 gaan we het spel doen beëindigen als we vaststellen dat een nieuw blokje niet past!

[← Stap 14](14.html) [Stap 16 →](16.html)
