# Intro &lt;mijnenveger&gt;

Hallo! Laten we Mijnenveger maken.

<div class="cartoon minesweeper"></div>

## Overzicht

1. [index.html](01.html)
2. [css grid](02.html)
3. [game loop](03.html)
4. [wat is Mijnenveger](04.html)

We hebben voor Mijnenveger eigenlijk gewoon een rooster nodig, en dit kan gemakkelijk met `css grid` en Javascript.

* Gebruik een code editor als Atom, VS Code, vim, ...
* Of kies een website zoals [jsbin](https://jsbin.com), [codepen](https://codepen.io), [stackblitz](https://stackblitz.com), [codesandbox](https://codesandbox.io), [jsfiddle](https://jsfiddle.net), ... waar je aan je code kan werken.
  Denk eraan: als je je bestanden online wil opslaan, heb je wellicht een account nodig.


## Wat moet ik kennen?

(Er komt hier ergens ooit een lijstje met links naar wat ze bij CoderDojo "sushi" noemen: kleine hapjes leerstof. Stay tuned.)

Dit stappenplan is niet voor beginners.

## Krijg ik feedback?

(Misschien kunnen we hier iets bedenken om voor jezelf beter te oordelen of je bepaalde dingen onder de knie krijgt. Misschien maken we ook wel een quiz! Stay tuned.)

CoderDojo's zijn eigenlijk snel voorbij.
Samen met de coach stellen we hopelijk regelmatig deze oriënterende vragen:

* wat hebben we gedaan?
* wat is het doel?
* wat is de volgende stap?
* <del>is er nog koffie?</del>

Om voor je eigen plezier te blijven programmeren, moet je leren volharden in het stellen van die vragen.
Het is ook leuk om samen aan code te leren werken met behulp van `git`, en elkaar feedback te geven in een `issuetracker`.

Het is leuk om nieuwe kennis in andere projecten toe te kunnen passen.
Je kan alle ideeën en creaties tijdens een CoderDojo ter sprake brengen!

## Hoe los ik problemen op?

Lees [Web Console](../learn/web/console.html) en gebruik `console.log()` in je code om de inhoud van variabelen te tonen in de console.

### CORS policy

<div class="cartoon helpless">CORS policy?</div>

Het zou kunnen dat je in stap 3 deze foutmelding krijgt:

```text
Access to script at 'file://...' from origin 'null'
has been blocked by CORS policy:
Cross origin requests are only supported
for protocol schemes: http, data, chrome,
chrome-extension, chrome-untrusted, https.
```

Als je offline werkt, moet je `index.html` op je computer *hosten* (een plaatselijke server), zodat je browser het `http` protocol gebruikt.

* Je het commando `python -m http.server 5500 -b 127.0.0.1` gebruiken in de opdrachtregel `cmd.exe`, als python is geïnstalleerd.
* Of je kan [deze plugin gebruiken](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) als je met Visual Studio Code werkt.
  Klik dan met je rechter muisknop op `index.html` en dan op "Open met Live Server".

Ga dan in de browser naar [http://127.0.0.1:5500](http://127.0.0.1:5500).

Lukt het niet? Kijk even hier: <a href="https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server" target="blank">Set up a local testing server</a>.
Als het niet meteen lukt om een server te starten, kan je alle Javascript in één bestand zetten, maar denk er dan aan om de prototype functies (`class`) bovenaan te zetten.

### Compatibiliteit

Werken de voorbeelden in de `<iframe>`s niet?

<div class="cartoon frustrated"></div>

* Oudere browsers kennen `css grid` niet. Je kan [op caniuse.com](https://caniuse.com) ("kan ik dit gebruiken") opzoeken wat browsers ondersteunen.
* Sommige online code editors herkennen `css grid` niet, ongeacht de browser die je gebruikt, omdat die je invoer soms eerst analyseren en "opkuisen".
  Probeer dan een andere hoeveelheid dan `fr`, bijvoorbeeld `20px` te gebruiken: `grid-template-rows: repeat(21, 20px);`.

### Waar vind ik een voorbeeld van de bestanden voor de huidige stap?

Vervang het laatste deel van de URL: https://cdjeeklo.gitlab.io/html/mijnenveger/<del>06.html</del><ins>06/spel.js</ins>

Of ga naar de broncode van deze site [op gitlab.com](https://gitlab.com/cdjeeklo/html/-/tree/main/mijnenveger).

### Wat met problemen die ik maar niet kan oplossen?

Beschrijf je probleem en copy-paste eventuele foutmeldingen op [discord](https://discord.gg/Dgc5zSc) of in de chat van html1 of html2 op MS Teams.
Je kan [op gitlab.com](https://gitlab.com/cdjeeklo/html/-/issues) ook je probleem beschrijven als je daar een account hebt (of een issue mailen naar het adres dat je onderaan op die pagina vindt).

## Zie ook

<div class="cartoon lookahead"></div>

[Overzicht](../) [Stap 1 →](01.html)
