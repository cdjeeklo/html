const spel = document.getElementById('spel');
const teOntmijnen = document.querySelector('#status > span')
const status = document.getElementById('status');
let breedte = 10;
let bommen  = 20;
let vlaggen = 0;
let ruitjes = [];
let verloren = false;

// maak het spelbord
function maakSpel() {
  teOntmijnen.innerText = bommen;

  // maak een lijstje met het gewenst aantal bommen
  const bomLijst = Array(bommen).fill('bom');
  // maak een lijstje met het lege vakjes om het spelrooster te vullen
  const vakLijst = Array(breedte*breedte - bommen).fill('vakje');
  // voeg de twee lijstjes samen als spelrooster
  const spelLijst = vakLijst.concat(bomLijst);
  // haal nu de volgorde grondig door elkaar
  // zodat de bommen goed verspreid liggen
  let m = spelLijst.length;
  while (m) {
    const i = Math.floor(Math.random() * m--);
    [spelLijst[m], spelLijst[i]] = [spelLijst[i], spelLijst[m]];
  }

  for(let i = 0; i < breedte*breedte; i++) {
    const ruitje = document.createElement('div')
    ruitje.setAttribute('id', i)
    ruitje.classList.add(spelLijst[i])
    spel.appendChild(ruitje)
    ruitjes.push(ruitje)

    // normale klik
    ruitje.addEventListener('click', function(e) {
      klik(ruitje)
    })

    // rechter klik
    ruitje.oncontextmenu = function(e) {
      e.preventDefault()
      zetVlag(ruitje)
    }
  }

  spel.style.gridTemplateRows    = 'repeat(' + breedte + ',1fr)';
  spel.style.gridTemplateColumns = 'repeat(' + breedte + ',1fr)';
  document.body.style.fontSize   = 'calc(3vmin * '  + 20 / breedte + ')';

  // voeg nummers toe
  for (let i = 0; i < ruitjes.length; i++) {
    let totaal = 0
    const linkerRand  = (i % breedte === 0)
    const rechterRand = (i % breedte === breedte - 1)
    if (ruitjes[i].classList.contains('vakje')) {
      if (i > 0 && !linkerRand && ruitjes[i -1].classList.contains('bom')) totaal ++
      if (i > breedte - 1 && !rechterRand && ruitjes[i +1 -breedte].classList.contains('bom')) totaal ++
      if (i > breedte && ruitjes[i -breedte].classList.contains('bom')) totaal ++
      if (i > breedte + 1 && !linkerRand && ruitjes[i -1 -breedte].classList.contains('bom')) totaal ++
      if (i < breedte * breedte - 2 && !rechterRand && ruitjes[i +1].classList.contains('bom')) totaal ++
      if (i < breedte * breedte - breedte && !linkerRand && ruitjes[i -1 +breedte].classList.contains('bom')) totaal ++
      if (i < breedte * breedte - breedte - 2 && !rechterRand && ruitjes[i +1 +breedte].classList.contains('bom')) totaal ++
      if (i < breedte * breedte - breedte - 1 && ruitjes[i +breedte].classList.contains('bom')) totaal ++
      ruitjes[i].setAttribute('data', totaal)
    }
  }
}
maakSpel()

// zet een vlag met een rechter muisklik
function zetVlag(ruitje) {
  if (verloren) return
  if (!ruitje.classList.contains('open') && (vlaggen < bommen)) {
    if (!ruitje.classList.contains('vlag')) {
      ruitje.classList.add('vlag')
      ruitje.innerText = ' 🚩'
      vlaggen ++
      teOntmijnen.innerText = bommen - vlaggen
      gewonnen()
    } else {
      ruitje.classList.remove('vlag')
      ruitje.innerText = ''
      vlaggen --
      teOntmijnen.innerText = bommen - vlaggen
    }
  }
}

// Controlleer of het vakje reeds open is en of er een bom onder zit.
// Open het vakje en toon het aantal bommen in naburige vakjes,
// of beëindig het spel als het een bom is.
function klik(ruitje) {
  let huidig = ruitje.id
  if (verloren) return
  if (ruitje.classList.contains('open') || ruitje.classList.contains('vlag')) return
  if (ruitje.classList.contains('bom')) {
    gameOver(ruitje)
  } else {
    let totaal = ruitje.getAttribute('data')
    if (totaal !=0) {
      ruitje.classList.add('open')
      if (totaal == 1) ruitje.classList.add('een')
      if (totaal == 2) ruitje.classList.add('twee')
      if (totaal == 3) ruitje.classList.add('drie')
      if (totaal == 4) ruitje.classList.add('vier')
      ruitje.innerText = totaal
      return
    }
    controleerRuitje(ruitje, huidig)
  }
  ruitje.classList.add('open')
}


// controleer naburige ruitjes als een ruitje is geklikt.
function controleerRuitje(ruitje, huidig) {
  const linkerRand = (huidig % breedte === 0)
  const rechterRand = (huidig % breedte === breedte -1)

  // ?? replace with game loop timer window.requestAnimationFrame(lus);
  //
  // In Chrome and Firefox, the 5th successive callback call is clamped;
  // Safari clamps on the 6th call; in Edge its the 3rd one. Gecko started to
  // treat setInterval() like this in version 56 (it already did this with
  // setTimeout(); see below).
  window.setTimeout(() => {
    if (huidig > 0 && !linkerRand) {
      const nieuw = ruitjes[parseInt(huidig) -1].id
      //const nieuw = parseInt(huidig) - 1   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig > breedte - 1 && !rechterRand) {
      const nieuw = ruitjes[parseInt(huidig) +1 -breedte].id
      //const nieuw = parseInt(huidig) +1 -breedte   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig > breedte) {
      const nieuw = ruitjes[parseInt(huidig -breedte)].id
      //const nieuw = parseInt(huidig) -breedte   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig > breedte + 1 && !linkerRand) {
      const nieuw = ruitjes[parseInt(huidig) -1 -breedte].id
      //const nieuw = parseInt(huidig) -1 -breedte   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig < breedte * breedte - 2 && !rechterRand) {
      const nieuw = ruitjes[parseInt(huidig) +1].id
      //const nieuw = parseInt(huidig) +1   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig < breedte * breedte - breedte && !linkerRand) {
      const nieuw = ruitjes[parseInt(huidig) -1 +breedte].id
      //const nieuw = parseInt(huidig) -1 +breedte   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig < breedte * breedte - breedte - 2 && !rechterRand) {
      const nieuw = ruitjes[parseInt(huidig) +1 +breedte].id
      //const nieuw = parseInt(huidig) +1 +breedte   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
    if (huidig < breedte * breedte - breedte - 1) {
      const nieuw = ruitjes[parseInt(huidig) +breedte].id
      //const nieuw = parseInt(huidig) +breedte   ....refactor
      const nieuwRuitje = document.getElementById(nieuw)
      klik(nieuwRuitje)
    }
  }, 50);
}

// game over
function gameOver(ruitje) {
  status.style.color = 'orange'
  status.innerText = 'Verloren!'
  verloren = true

  // toon alle bommen
  ruitjes.forEach(ruitje => {
    if (ruitje.classList.contains('bom')) {
      ruitje.innerText = '💣'
      ruitje.classList.remove('bom')
      ruitje.classList.add('open')
    }
  })
}

// hebben we gewonnen?
function gewonnen() {
  let gevonden = 0

  for (let i = 0; i < ruitjes.length; i++) {
    if (ruitjes[i].classList.contains('vlag') && ruitjes[i].classList.contains('bom')) {
      gevonden ++
    }
    if (gevonden === bommen) {
      status.style.backgroundColor = 'black'
      status.style.color = 'darkgreen'
      status.innerText = 'Gewonnen!'
      verloren = true
    }
  }
}
