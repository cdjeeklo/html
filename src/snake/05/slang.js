class Slang {
  constructor(){
    this.lichaam = [
      { x: 11, y: 10 },
      { x: 11, y: 11 },
      { x: 11, y: 12 }
    ];
  }
  verschijn(spel) {
    this.lichaam.forEach(deel => {
      const element = document.createElement('div');
      element.style.gridColumnStart = deel.x;
      element.style.gridRowStart = deel.y;
      element.classList.add('slang');
      spel.appendChild(element);
    });
  }
}
export { Slang };
