import { Slang } from './slang.js'
let slang = new Slang();
let spel = document.getElementById('spel');
let vorigeTijd = 0;
const SNELHEID = 2;

function lus(nu) {
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  console.log("beweeg");
  vorigeTijd = nu;
  vernieuw();
  verschijn();
}
window.requestAnimationFrame(lus);

function vernieuw() {
  // is het slangenhoofd aan de prooi?
  // is het slangenhoofd aan de eigen staart?
  // is het slangenhoofd aan de rand?
  // waar komt een nieuwe prooi?
}

function verschijn() {
  // maak elementen voor de slang en de prooi?
  spel.innerHTML = "";
  slang.verschijn(spel);
}
