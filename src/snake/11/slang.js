class Slang {
  constructor(richting={x: 1, y: 0}){
    this.lichaam = [ { x: 11, y: 11 } ];
    this.richting = richting;
  }
  is_onder(pos, isHoofd=false) {
    return this.lichaam.some((deel, index) => {
      if (isHoofd && index === 0) return false;
      return deel.x === pos.x && deel.y === pos.y
    });
  }
  vernieuw() {
    for (let i = this.lichaam.length - 2; i >= 0; i--) {
      this.lichaam[i+1] = { ...this.lichaam[i] };
    }
    this.lichaam[0].x += this.richting.x;
    this.lichaam[0].y += this.richting.y;
    if (this.lichaam[0].x < 1) this.lichaam[0].x = 21;
    if (this.lichaam[0].y < 1) this.lichaam[0].y = 21;
    if (this.lichaam[0].x > 21) this.lichaam[0].x = 1;
    if (this.lichaam[0].y > 21) this.lichaam[0].y = 1;
    if (this.is_onder(this.lichaam[0], true)) this.lichaam = [];
  }
  verschijn(spel) {
    this.lichaam.forEach(deel => {
      const element = document.createElement('div');
      element.style.gridColumnStart = deel.x;
      element.style.gridRowStart = deel.y;
      element.classList.add('slang');
      spel.appendChild(element);
    });
  }
}
export { Slang };
