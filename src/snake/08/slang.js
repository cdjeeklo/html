class Slang {
  constructor(richting={x: 1, y: 0}){
    this.lichaam = [
      { x: 11, y: 10 },
      { x: 11, y: 11 },
      { x: 11, y: 12 },
      { x: 12, y: 12 },
      { x: 13, y: 12 }
    ];
    this.richting = richting;
  }
  is_onder(pos) {
    return this.lichaam.some(deel => {
      return deel.x === pos.x && deel.y === pos.y
    });
  }
  vernieuw() {
    for (let i = this.lichaam.length - 2; i >= 0; i--) {
      this.lichaam[i+1] = { ...this.lichaam[i] };
    }
    this.lichaam[0].x += this.richting.x;
    this.lichaam[0].y += this.richting.y;
  }
  verschijn(spel) {
    this.lichaam.forEach(deel => {
      const element = document.createElement('div');
      element.style.gridColumnStart = deel.x;
      element.style.gridRowStart = deel.y;
      element.classList.add('slang');
      spel.appendChild(element);
    });
  }
}
export { Slang };
