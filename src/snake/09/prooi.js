class Prooi {
  vernieuw(slang) {
    while (this.pos == null || slang.is_onder(this.pos)) {
      if (this.pos) {
        console.log("hmm yammie!");
        slang.lichaam.push({ ...slang.lichaam[slang.lichaam.length - 1]});
      }
      this.pos = {
        x: Math.floor(Math.random() * 20 + 1),
        y: Math.floor(Math.random() * 20 + 1)
      }
    }
  }
  verschijn(spel) {
    const element = document.createElement('div');
    element.style.gridColumnStart = this.pos.x;
    element.style.gridRowStart = this.pos.y;
    element.classList.add('prooi');
    spel.appendChild(element);
  }
}
export { Prooi };
