# Botsen!

## De rand

Onze `slang.vernieuw()` functie ziet er zo uit:

```javascript
vernieuw() {
  for (let i = this.lichaam.length - 2; i >= 0; i--) {
    this.lichaam[i+1] = { ...this.lichaam[i] };
  }
  this.lichaam[0].x += this.richting.x;
  this.lichaam[0].y += this.richting.y;
}
```

Om de slang van de ene rand van het rooster naar de tegenovergestelde te teleporteren, doen we dit:

```javascript
if (this.lichaam[0].x < 1) this.lichaam[0].x = 21;
if (this.lichaam[0].y < 1) this.lichaam[0].y = 21;
if (this.lichaam[0].x > 21) this.lichaam[0].x = 1;
if (this.lichaam[0].y > 21) this.lichaam[0].y = 1;
```

## Botsing met het eigen lichaam

Is de slang tegen het eigen lichaam?

Kunnen we hiervoor de functie `is_onder()` gebruiken, met het hoofd van de slang als parameter?
Neen, omdat het hoofd deel uitmaakt van het lijstje `this.lichaam`, en de positie van het hoofd is dus altijd een positie in het lijstje.

Maar we kunnen de functie aanpassen met een extra *argument* `isHoofd` om het hoofd (`this.lichaam[0]`) over te slaan.

```javascript
is_onder(pos, isHoofd=false) {
  return this.lichaam.some((deel, index) => {
    if (isHoofd && index === 0) return false;
    return deel.x === pos.x && deel.y === pos.y
  });
}
```

- Je kan bij de *arguments* een standaard waarde (`false`) geven.
  Wanneer we alleen de parameter `pos` geven, zal `isHoofd` `false` zijn.
- We hadden het al eerder over de *arrow syntax*.
  Om twee of meer *arguments* te gebruiken, zetten we ze in haakjes: "`(deel, index) => {}`".

Als we nu `this.is_onder(this.lichaam[0], true)` gebruiken, is `isHoofd` `true` en zal het antwoord op de vraag of het eerste deel het lichaam raakt altijd `false` zijn.
Als de slang het eigen lichaam raakt, maken we `this.lichaam` leeg om aan te geven dat het spel gedaan is.

```javascript
vernieuw() {
  // ...
  if (this.lichaam[0].y > 21) this.lichaam[0].y = 1;
  if (this.is_onder(this.lichaam[0], true)) this.lichaam = [];
}
```

We **stoppen** dus de `lus()` als `this.lichaam` leeg is. (met `return`)

```javascript
function lus(nu) {
  if (slang.lichaam.length == 0) return;
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  vorigeTijd = nu;
  vernieuw();
  verschijn();
}
```

Zo vragen we aan de speler of die opnieuw wil spelen:

```javascript
function lus(nu) {
  if (slang.lichaam.length == 0) {
    if (confirm('Verloren! Druk ok om opnieuw te spelen.')) {
      window.location.reload();
    }
    return
  }
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  vorigeTijd = nu;
  vernieuw();
  verschijn();
}
```

Als we het spel ook willen beëindigen wanneer we de rand raken, kunnen we in `slang.vernieuw()` dit doen:

```javascript
if (this.lichaam[0].x < 1
 || this.lichaam[0].y < 1
 || this.lichaam[0].x > 21
 || this.lichaam[0].y > 21) {
  this.lichaam = [];
}
```

Onze slang kan nu eten, sterven en teleporteren.

<iframe loading="lazy" src="09/index.html" height="250px" width="250px" frameBorder="0"></iframe>

In stap 10 voegen we nog knoppen toe om het spel met een **aanraakscherm** te kunnen spelen.

[← Stap 8](08.html) [Stap 10 →](10.html)
