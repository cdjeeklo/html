import { Slang } from './slang.js'
let slang = new Slang();
let spel = document.getElementById('spel');
let vorigeTijd = 0;
const SNELHEID = 4;

function lus(nu) {
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  console.log("beweeg");
  vorigeTijd = nu;
  vernieuw();
  verschijn();
}
window.requestAnimationFrame(lus);

function vernieuw() {
  // waar komt een nieuwe prooi?
  slang.vernieuw();
}

function verschijn() {
  // maak elementen voor de slang en de prooi?
  spel.innerHTML = "";
  slang.verschijn(spel);
}

window.addEventListener('keydown', e => {
  switch (e.key) {
    case 'ArrowUp':
      e.preventDefault();
      if (slang.richting.y !== 0) break;
      slang.richting = { x:  0, y: -1 };
      break;
    case 'ArrowRight':
      e.preventDefault();
      if (slang.richting.x !== 0) break;
      slang.richting = { x:  1, y:  0 };
      break;
    case 'ArrowDown':
      e.preventDefault();
      if (slang.richting.y !== 0) break;
      slang.richting = { x:  0, y:  1 };
      break;
    case 'ArrowLeft':
      e.preventDefault();
      if (slang.richting.x !== 0) break;
      slang.richting = { x: -1, y:  0 };
      break;
  }
});
