import { Slang } from './slang.js'
import { Prooi } from './prooi.js'
let slang = new Slang();
let prooi = new Prooi();
prooi.vernieuw(slang);
let spel = document.getElementById('spel');
let vorigeTijd = 0;
const SNELHEID = 4;

function lus(nu) {
  if (slang.lichaam.length == 0) {
    if (confirm('Verloren! Druk ok om opnieuw te spelen.')) {
      window.location.reload();
    }
    return
  }
  window.requestAnimationFrame(lus);
  if ((nu - vorigeTijd) < 1000 / SNELHEID) return;
  vorigeTijd = nu;
  vernieuw();
  verschijn();
}
window.requestAnimationFrame(lus);

function vernieuw() {
  // waar komt een nieuwe prooi?
  slang.vernieuw();
  prooi.vernieuw(slang);
}

function verschijn() {
  // maak elementen voor de slang en de prooi?
  spel.innerHTML = "";
  slang.verschijn(spel);
  prooi.verschijn(spel);
}

let boven = document.getElementById('boven');
boven.addEventListener('touchstart', e => {
  e.preventDefault();
  if (slang.richting.y !== 0) return;
  slang.richting = { x:  0, y: -1 };
});

let links = document.getElementById('links');
links.addEventListener('touchstart', e => {
  e.preventDefault();
  if (slang.richting.x !== 0) return;
  slang.richting = { x: -1, y:  0 };
});

let rechts = document.getElementById('rechts');
rechts.addEventListener('touchstart', e => {
  e.preventDefault();
  if (slang.richting.x !== 0) return;
  slang.richting = { x:  1, y:  0 };
});

let beneden = document.getElementById('beneden');
beneden.addEventListener('touchstart', e => {
  e.preventDefault();
  if (slang.richting.y !== 0) return;
  slang.richting = { x:  0, y:  1 };
});

window.addEventListener('keydown', e => {
  switch (e.key) {
    case ' ':
      e.preventDefault();
      break;
    case 'ArrowUp':
      e.preventDefault();
      if (slang.richting.y !== 0) break;
      slang.richting = { x:  0, y: -1 };
      break;
    case 'ArrowRight':
      e.preventDefault();
      if (slang.richting.x !== 0) break;
      slang.richting = { x:  1, y:  0 };
      break;
    case 'ArrowDown':
      e.preventDefault();
      if (slang.richting.y !== 0) break;
      slang.richting = { x:  0, y:  1 };
      break;
    case 'ArrowLeft':
      e.preventDefault();
      if (slang.richting.x !== 0) break;
      slang.richting = { x: -1, y:  0 };
      break;
  }
});
