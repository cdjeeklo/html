SOURCE=src/
TARGET=public/
TOOLS=tools/
MD_FILES=$(wildcard $(SOURCE)**/*.md)
EXT=md_in_html fenced_code codehilite markdown_checklist.extension attr_list markdown-video:VideoExtension

GEVORDERD=snake
EXPERT=tetris mijneveger

PERCENT:=%
GRAAD=starter
$(patsubst %,$(TARGET)%/$(PERCENT),$(GEVORDERD)) : GRAAD=gevorderd
$(patsubst %,$(TARGET)%/$(PERCENT),$(EXPERT)) : GRAAD=expert

.PHONY: links p5demos clean

all: .cmd_ok $(patsubst $(SOURCE)%.md,$(TARGET)%.html,$(MD_FILES)) $(TARGET)dark.css $(TARGET)light.css

.cmd_ok:
	./tools/check-cmds.sh

links: p5demos
	find $(SOURCE) -mindepth 2 -type d -not -empty -exec sh -c 'mkdir -p $(TARGET)$${0#*/} && ln -f $$0/* $(TARGET)$${0#*/}' {} \;

p5demos:
	find $(SOURCE)p5js -type f -name '*.md' -exec $(TOOLS)p5demos.sh {} \;

clean:
	find $(TARGET) -mindepth 2 -type f -name '*.html' -delete
	find $(TARGET) -mindepth 2 -type d -empty -delete

$(TARGET)%.html: $(SOURCE)%.md
	$(info $(patsubst $(TARGET)%/,%,$(dir $@)) $(GRAAD) $(notdir $@))
	@mkdir -p $(dir $@)
	@cp -f $(SOURCE)head.html $@
	@PYTHONPATH=./tools markdown_py -e UTF-8 $(patsubst %,-x %,$(EXT)) $< 2>/dev/null >> $@
	@sed -f $(SOURCE)content.sed -i $@
	@sed -i 's/<body>/<body class="${GRAAD}">/' $@
	@sed -i 's/titel/$(patsubst $(TARGET)%/,%,$(dir $@))/' $@
	@cat $(SOURCE)tail.html >> $@

$(TARGET)dark.css:
	pygmentize -S solarized-dark -f html > $@

$(TARGET)light.css:
	pygmentize -S solarized-light -f html > $@

