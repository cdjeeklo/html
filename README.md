CoderDojo Eeklo HTML track
==========================

[This site](https://cdjeeklo.gitlab.io/html/) will present Dutch HTML, CSS and Javascript tutorials and game creation challenges from our CoderDojo in Eelko, Belgium.

Op [deze site](https://cdjeeklo.gitlab.io/html/) komen Nederlandstalige leerpaden voor HTML, CSS en Javascript en uitleg voor het maken van spelletjes van de CoderDojo in Eeklo, België.

## contributions - bijdragen

You can use the WebIDE button on the top of this page to edit the source files online and prepare a commit, which you save on a new branch which you can then request to be merged into *main*.
Only maintainers can commit their changes upstream directly to *main*.
Gitlab's continuous integration tool has set a post-receive hook on that branch, so that the markdown sources will be converted to html with python-markdown (in a docker container on the cloud).
The resulting pages are then copied and hosted.

Om de bronbestanden te bewerken en een commit voor te bereiden kan je bovenaan een "WebIDE" knop klikken.
Je kan je commit dan "opslaan" met een beschrijving op een nieuwe branch en vragen (met een *merge request*) naar een maintainer om je wijzigingen toe te voegen aan de *main* branch.
Enkel maintainers kunnen de *main* branch rechtstreeks wijzigen.
Telkens als die verandert, zal Gitlab de markdown pagina's in html omzetten en hosten.

The file "public/girl.svg" is partially created by hand (see below).

## requisites - benodigdheden

### building pages locally - pagina's plaatselijk genereren

* [python-markdown](https://python-markdown.github.io)
* [make](https://www.gnu.org/software/make)
* [sed](https://www.gnu.org/software/sed)
* [git-lfs](https://git-lfs.github.com)

If you want to build the pages locally, you'll need the above programs.

Enkel als je de html pagina's plaatselijk wil genereren, heb de bovenstaande programma's nodig.

```sh
$ git clone https://gitlab.com/cdjeeklo/html.git
$ git lfs checkout
$ cd html
$ make
snake gevorderd 01.html
...
$ python -m http.server -b 127.0.0.1 -d public 8001 &>/dev/null &
$ firefox 127.0.0.1:8001 &>/dev/null &
```

(Elke regel die met "$" begint is een opdrachtregel. Je moet waarschijnlijk in Windows `cmd.exe` gebruiken en `&>/dev/null` weglaten?)

### adding a cartoon - een cartoon toevoegen

Please make a new cartoon as a layer in "src/girl-inkscape.svg" using Inkscape (version 1.0 or newer).
Give the layer a name, make it the only visible layer and save the file when done.

You should then run [svgcleaner](https://github.com/RazrFalcon/SVGCleaner) to prepare your newly created cartoon so that it can be manually added to `public/girl.svg`. To do this, use the [tools/cartoon.sh](tools/cartoon.sh) bash script, which makes a copy of "src/girl-inkscape.svg" with all of the layer groups except the one with the `inkscape:label` you just created.

Now, copy the contents of `src/girl-inkscape.svg-clean.svg` to `public/girl.svg`, please put it after the first group, which is the table, chair and laptop.
Fix any errors and identify the paths that need to be styled differently.
Remove all remaining stroke and display properties from your cartoon.
Use classes to do so (add more css classes as needed).

Add this to "public/content.css":

```css
.cartoon.your-label{
  background-image: url('../girl.svg#your-label');
}
```

and use `<div class="cartoon your-label"></div>` in the markdown where you want it to appear.

Finally, stage "public/girl.svg", "src/girl-inkscape.svg" and "public/content.css" and commit it with the word cartoon in the message.
